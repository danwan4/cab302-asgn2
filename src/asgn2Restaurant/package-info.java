/**
 * This package contains classes that act as a bridge between the GUI, the log
 * files and the classes in the asgn2Customers and the asgn2Pizzas packages.
 * 
 * @author Jason Chang and Daniel Wan
 *
 */
package asgn2Restaurant;