package asgn2Restaurant;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.time.LocalTime;
import java.time.format.DateTimeParseException;
import java.util.ArrayList;
import asgn2Customers.Customer;
import asgn2Customers.CustomerFactory;
import asgn2Exceptions.CustomerException;
import asgn2Exceptions.LogHandlerException;
import asgn2Exceptions.PizzaException;
import asgn2Pizzas.Pizza;
import asgn2Pizzas.PizzaFactory;

/**
 *
 * A class that contains methods that use the information in the log file to
 * return Pizza and Customer object - either as an individual Pizza/Customer
 * object or as an ArrayList of Pizza/Customer objects.
 * 
 * @author Jason Chang and Daniel Wan
 *
 */
public class LogHandler {

	final static String COMMA = ",";

	/**
	 * Returns an ArrayList of Customer objects from the information contained
	 * in the log file ordered as they appear in the log file.
	 * 
	 * @param filename
	 *            - The file name of the log file
	 * @return An ArrayList of Customer objects from the information contained
	 *         in the log file ordered as they appear in the log file.
	 * @throws CustomerException
	 *             If the log file contains semantic errors leading that violate
	 *             the customer constraints listed in Section 5.3 of the
	 *             Assignment Specification or contain an invalid customer code
	 *             (passed by another class).
	 * @throws LogHandlerException
	 *             If there was a problem with the log file not related to the
	 *             semantic errors above.
	 * 
	 */
	public static ArrayList<Customer> populateCustomerDataset(String filename)
			throws CustomerException, LogHandlerException {
		BufferedReader reader;

		// Open file
		try {
			reader = new BufferedReader(new FileReader(filename));
		} catch (FileNotFoundException e) {
			throw new LogHandlerException(e.getMessage());
		}

		ArrayList<Customer> customers = new ArrayList<Customer>();

		// Read lines and create Customers
		String line = null;
		try {
			while ((line = reader.readLine()) != null) {
				customers.add(createCustomer(line));
			}
		} catch (IOException e) {
			throw new LogHandlerException(e.getMessage());
		}

		// Close reader
		try {
			reader.close();
		} catch (IOException e) {
			throw new LogHandlerException(e.getMessage());
		}

		return customers;
	}

	/**
	 * Returns an ArrayList of Pizza objects from the information contained in
	 * the log file ordered as they appear in the log file. .
	 * 
	 * @param filename
	 *            - The file name of the log file
	 * @return An ArrayList of Pizza objects from the information contained in
	 *         the log file ordered as they appear in the log file.
	 * @throws PizzaException
	 *             If the log file contains semantic errors leading that violate
	 *             the pizza constraints listed in Section 5.3 of the Assignment
	 *             Specification or contain an invalid pizza code (passed by
	 *             another class).
	 * @throws LogHandlerException
	 *             If there was a problem with the log file not related to the
	 *             semantic errors above.
	 * 
	 */
	public static ArrayList<Pizza> populatePizzaDataset(String filename) throws PizzaException, LogHandlerException {
		BufferedReader reader;
		ArrayList<Pizza> Pizzas = new ArrayList<Pizza>();

		// Open file
		try {
			reader = new BufferedReader(new FileReader(filename));
		} catch (FileNotFoundException e) {
			throw new LogHandlerException(e.getMessage());
		}

		// Read lines and create Pizzas
		String line = null;
		try {
			while ((line = reader.readLine()) != null) {
				Pizzas.add(createPizza(line));
			}
		} catch (IOException e) {
			throw new LogHandlerException(e.getMessage());
		}

		// Close reader
		try {
			reader.close();
		} catch (IOException e) {
			throw new LogHandlerException(e.getMessage());
		}

		return Pizzas;
	}

	/**
	 * Creates a Customer object by parsing the information contained in a
	 * single line of the log file. The format of each line is outlined in
	 * Section 5.3 of the Assignment Specification.
	 * 
	 * @param line
	 *            - A line from the log file
	 * @return A Customer object containing the information from the line in the
	 *         log file
	 * @throws CustomerException
	 *             If the log file contains semantic errors leading that violate
	 *             the customer constraints listed in Section 5.3 of the
	 *             Assignment Specification or contain an invalid customer code
	 *             (passed by another class).
	 * @throws LogHandlerException
	 *             If there was a problem parsing the line from the log file.
	 */
	public static Customer createCustomer(String line) throws CustomerException, LogHandlerException {
		String[] orderArray = line.split(COMMA);

		if (orderArray.length != 9) {
			throw new LogHandlerException("Incorrect number of values in line");
		}

		String name = orderArray[2];
		String mobileNumber = orderArray[3];
		String customerCode = orderArray[4];
		int locationX;
		int locationY;
		try {
			locationX = Integer.parseInt(orderArray[5]);
			locationY = Integer.parseInt(orderArray[6]);
		} catch (NumberFormatException e) {
			throw new LogHandlerException(e.getMessage());
		}

		return CustomerFactory.getCustomer(customerCode, name, mobileNumber, locationX, locationY);
	}

	/**
	 * Creates a Pizza object by parsing the information contained in a single
	 * line of the log file. The format of each line is outlined in Section 5.3
	 * of the Assignment Specification.
	 * 
	 * @param line
	 *            - A line from the log file
	 * @return A Pizza object containing the information from the line in the
	 *         log file
	 * @throws PizzaException
	 *             If the log file contains semantic errors leading that violate
	 *             the pizza constraints listed in Section 5.3 of the Assignment
	 *             Specification or contain an invalid pizza code (passed by
	 *             another class).
	 * @throws LogHandlerException
	 *             If there was a problem parsing the line from the log file.
	 */
	public static Pizza createPizza(String line) throws PizzaException, LogHandlerException {
		String[] orderArray = line.split(COMMA);

		if (orderArray.length != 9) {
			throw new LogHandlerException("Incorrect number of values in line");
		}

		LocalTime orderTime;
		LocalTime deliveryTime;
		try {
			orderTime = LocalTime.parse(orderArray[0]);
			deliveryTime = LocalTime.parse(orderArray[1]);
		} catch (DateTimeParseException e) {
			throw new LogHandlerException(e.getMessage());
		}
		String pizzaCode = orderArray[7];

		int quantity;
		try {
			quantity = Integer.parseInt(orderArray[8]);
		} catch (NumberFormatException e) {
			throw new LogHandlerException(e.getMessage());
		}

		return PizzaFactory.getPizza(pizzaCode, quantity, orderTime, deliveryTime);
	}

}
