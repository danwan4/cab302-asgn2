package asgn2GUIs;

import java.util.ArrayList;

import javax.swing.table.AbstractTableModel;

import asgn2Pizzas.Pizza;

/**
 * A table model class for Pizzas.
 * 
 * @author Jason Chang and Daniel Wan
 *
 */
public class PizzaTableModel extends AbstractTableModel {

	private String[] columnNames = { "Pizza", "Quantity", "Price", "Cost", "Profit" };

	private ArrayList<Pizza> pizzas;

	/**
	 * Creates a new Pizza Table Model.
	 */
	public PizzaTableModel() {
		pizzas = new ArrayList<Pizza>();
	}

	@Override
	public int getColumnCount() {
		return columnNames.length;
	}

	@Override
	public String getColumnName(int column) {
		return columnNames[column];
	}

	@Override
	public int getRowCount() {
		return pizzas.size();
	}

	@Override
	public Object getValueAt(int row, int column) {
		Pizza pizza = pizzas.get(row);

		switch (column) {
		case 0:
			return pizza.getPizzaType();
		case 1:
			return pizza.getQuantity();
		case 2:
			return String.format("$%.2f", pizza.getOrderPrice());
		case 3:
			return String.format("$%.2f", pizza.getOrderCost());
		case 4:
			return String.format("$%.2f", pizza.getOrderProfit());
		default:
			return null;
		}
	}

	/**
	 * Adds a pizza to the table model.
	 * 
	 * @param pizza
	 *            - Pizza to add to the table.
	 */
	public void add(Pizza pizza) {
		pizza.calculateCostPerPizza();
		pizzas.add(getRowCount(), pizza);
	}

	/**
	 * Remove all pizzas from the table model.
	 */
	public void reset() {
		pizzas.clear();
	}

}
