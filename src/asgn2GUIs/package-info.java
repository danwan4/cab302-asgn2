/**
 * This package contains the GUI which can be used to interact with the rest of the system. 
 * 
 * @author Jason Chang and Daniel Wan
 *
 */
package asgn2GUIs;