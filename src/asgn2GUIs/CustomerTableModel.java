package asgn2GUIs;

import java.util.ArrayList;

import javax.swing.table.AbstractTableModel;

import asgn2Customers.Customer;

/**
 * A table model class for Customers.
 * 
 * @author Jason Chang and Daniel Wan
 *
 */
public class CustomerTableModel extends AbstractTableModel {

	private String[] columnNames = { "Name", "Mobile", "Type", "Location", "Distance" };

	private ArrayList<Customer> customers;

	/**
	 * Creates a new Customer Table Model.
	 */
	public CustomerTableModel() {
		customers = new ArrayList<Customer>();
	}

	@Override
	public int getColumnCount() {
		return columnNames.length;
	}

	@Override
	public String getColumnName(int column) {
		return columnNames[column];
	}

	@Override
	public int getRowCount() {
		return customers.size();
	}

	@Override
	public Object getValueAt(int row, int column) {
		Customer customer = customers.get(row);

		switch (column) {
		case 0:
			return customer.getName();
		case 1:
			return customer.getMobileNumber();
		case 2:
			return customer.getCustomerType();
		case 3:
			return Integer.toString(customer.getLocationX()) + ", " + Integer.toString(customer.getLocationY());
		case 4:
			return String.format("%.2f", customer.getDeliveryDistance()) + " km";
		default:
			return null;
		}
	}

	/**
	 * Adds a customer to the table model.
	 * 
	 * @param customer
	 *            - Customer to add to the table.
	 */
	public void add(Customer customer) {
		customers.add(getRowCount(), customer);
	}

	/**
	 * Remove all customers from the table model.
	 */
	public void reset() {
		customers.clear();
	}

}
