package asgn2GUIs;

import java.awt.event.ActionEvent;

import java.awt.event.ActionListener;
import java.io.File;

import asgn2Exceptions.CustomerException;
import asgn2Exceptions.LogHandlerException;
import asgn2Exceptions.PizzaException;
import asgn2Restaurant.PizzaRestaurant;

import java.awt.*;
import javax.swing.*;

/**
 * This class is the graphical user interface for the rest of the system.
 * Currently it is a �dummy� class which extends JFrame and implements Runnable
 * and ActionLister. It should contain an instance of an
 * asgn2Restaurant.PizzaRestaurant object which you can use to interact with the
 * rest of the system. You may choose to implement this class as you like,
 * including changing its class signature � as long as it maintains its core
 * responsibility of acting as a GUI for the rest of the system. You can also
 * use this class and asgn2Wizards.PizzaWizard to test your system as a whole
 * 
 * 
 * @author Jason Chang and Daniel Wan
 *
 */
public class PizzaGUI extends javax.swing.JFrame implements Runnable, ActionListener {

	public static final int WIDTH = 1000;
	public static final int HEIGHT = 560;

	private PizzaRestaurant restaurant;

	private JFileChooser filechooser;

	private JPanel pnlTop;
	private JPanel pnlBottom;
	private JPanel pnlLeft;
	private JPanel pnlRight;
	private JPanel pnlDisplay;
	private JPanel pnlTotalDistance;
	private JPanel pnlTotalProfit;

	private JLabel lblCustomers;
	private JLabel lblPizzas;
	private JLabel lblTotalDistance;
	private JLabel lblTotalProfit;

	private JScrollPane paneCustomers;
	private JScrollPane panePizzas;

	private JTable tblCustomers;
	private JTable tblPizzas;

	private CustomerTableModel mdlCustomers;
	private PizzaTableModel mdlPizzas;

	private JTextField txfTotalDistance;
	private JTextField txfTotalProfit;

	private JButton btnLoad;
	private JButton btnShow;
	private JButton btnCalculate;
	private JButton btnReset;

	/**
	 * Creates a new Pizza GUI with the specified title.
	 * 
	 * @param title
	 *            - The title for the supertype JFrame
	 */
	public PizzaGUI(String title) {
		this.setTitle(title);
		setSize(WIDTH, HEIGHT);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		// Create objects
		restaurant = new PizzaRestaurant();
		filechooser = new JFileChooser();
		createPanels();
		createLabels();
		createTables();
		createTextFields();
		createButtons();

		// Lay out the panels
		this.getContentPane().add(pnlTop, BorderLayout.NORTH);
		this.getContentPane().add(pnlBottom, BorderLayout.SOUTH);
		this.getContentPane().add(pnlLeft, BorderLayout.WEST);
		this.getContentPane().add(pnlRight, BorderLayout.EAST);
		this.getContentPane().add(pnlDisplay, BorderLayout.CENTER);

		// Add stuff to and lay out display (center) panel
		pnlDisplay.setLayout(new GridBagLayout());
		GridBagConstraints constraints = new GridBagConstraints();
		addToPanel(pnlDisplay, constraints, lblCustomers, 0, 0, 1, 1);
		addToPanel(pnlDisplay, constraints, lblPizzas, 1, 0, 1, 1);
		addToPanel(pnlDisplay, constraints, paneCustomers, 0, 1, 1, 1);
		addToPanel(pnlDisplay, constraints, panePizzas, 1, 1, 1, 1);
		addToPanel(pnlDisplay, constraints, pnlTotalDistance, 0, 2, 1, 1);
		addToPanel(pnlDisplay, constraints, pnlTotalProfit, 1, 2, 1, 1);

		// Add stuff to and lay out distance panel
		pnlTotalDistance.setLayout(new FlowLayout());
		pnlTotalDistance.add(lblTotalDistance);
		pnlTotalDistance.add(txfTotalDistance);
		pnlTotalDistance.setVisible(false); // Initially invisble

		// Add stuff to and lay out profit panel
		pnlTotalProfit.setLayout(new FlowLayout());
		pnlTotalProfit.add(lblTotalProfit);
		pnlTotalProfit.add(txfTotalProfit);
		pnlTotalProfit.setVisible(false); // Initially invisble

		// Add buttons to the bottom panel
		pnlBottom.setLayout(new FlowLayout());
		pnlBottom.add(btnLoad);
		pnlBottom.add(btnShow);
		pnlBottom.add(btnCalculate);
		pnlBottom.add(btnReset);

		// Disable buttons that don't do anything at start
		btnLoad.setEnabled(true);
		btnCalculate.setEnabled(false);
		btnShow.setEnabled(false);
		btnReset.setEnabled(false);

		// Show it
		repaint();
		this.setVisible(true);
	}

	private void createPanels() {
		pnlTop = createPanel(Color.LIGHT_GRAY);
		pnlBottom = createPanel(Color.LIGHT_GRAY);
		pnlLeft = createPanel(Color.LIGHT_GRAY);
		pnlRight = createPanel(Color.LIGHT_GRAY);
		pnlDisplay = createPanel(Color.WHITE);
		pnlTotalDistance = createPanel(Color.WHITE);
		pnlTotalProfit = createPanel(Color.WHITE);
	}

	/**
	 * Create a panel with a certain background colour.
	 * 
	 * @param colour
	 *            - Background colour.
	 * @return New panel with background colour set.
	 */
	private JPanel createPanel(Color colour) {
		JPanel panel = new JPanel();
		panel.setBackground(colour);
		return panel;
	}

	/**
	 * Add a component to a panel using given grid bag constraints and layout
	 * locations.
	 *
	 * @param panel
	 *            - Panel to add component to.
	 * @param constraints
	 *            - Layout Constraints for components.
	 * @param component
	 *            - The component to add.
	 * @param x
	 *            - The x grid position.
	 * @param y
	 *            - The y grid position.
	 * @param w
	 *            - The grid width of the component.
	 * @param h
	 *            - The grid height of the component.
	 */
	private void addToPanel(JPanel panel, GridBagConstraints constraints, Component component, int x, int y, int w, int h) {
		constraints.gridx = x;
		constraints.gridy = y;
		constraints.gridwidth = w;
		constraints.gridheight = h;
		panel.add(component, constraints);
	}

	private void createLabels() {
		lblCustomers = new JLabel("Customers");
		lblPizzas = new JLabel("Orders");
		lblTotalDistance = new JLabel("Total Delivery Distance: ");
		lblTotalProfit = new JLabel("Total Profit: ");
	}

	private void createTables() {
		// Customers Table
		mdlCustomers = new CustomerTableModel();
		tblCustomers = new JTable();
		tblCustomers.setModel(mdlCustomers);
		tblCustomers.setFillsViewportHeight(true);
		paneCustomers = new JScrollPane(tblCustomers);

		// Pizzas Table
		mdlPizzas = new PizzaTableModel();
		tblPizzas = new JTable();
		tblPizzas.setModel(mdlPizzas);
		tblPizzas.setFillsViewportHeight(true);
		panePizzas = new JScrollPane(tblPizzas);
	}

	private void resetTables() {
		mdlCustomers.reset();
		mdlCustomers.fireTableDataChanged();
		mdlPizzas.reset();
		mdlPizzas.fireTableDataChanged();
	}

	private void createTextFields() {
		txfTotalDistance = new JTextField();
		txfTotalDistance.setEditable(false);
		txfTotalProfit = new JTextField();
		txfTotalProfit.setEditable(false);
	}

	private void createButtons() {
		btnLoad = createButton("Load Log");
		btnShow = createButton("Show Log");
		btnCalculate = createButton("Calculate Totals");
		btnReset = createButton("Reset");
	}

	/**
	 * Create a button with the specified text and an action listener.
	 * 
	 * @param text
	 *            - Button text.
	 * @return Button with text and action listener added.
	 */
	private JButton createButton(String text) {
		JButton button = new JButton(text);
		button.addActionListener(this);
		return button;
	}

	@Override
	public void actionPerformed(ActionEvent event) {
		Object source = event.getSource();

		if (source == btnLoad) {
			btnLoadHandler();
		} else if (source == btnShow) {
			btnShowHandler();
		} else if (source == btnCalculate) {
			btnCalculateHandler();
		} else if (source == btnReset) {
			btnResetHandler();
		}
	}

	/**
	 * Handler for when the load button is pressed.
	 */
	private void btnLoadHandler() {
		int retVal = filechooser.showOpenDialog(this);
		if (retVal == JFileChooser.APPROVE_OPTION) {
			File file = filechooser.getSelectedFile();
			try {
				restaurant.processLog(file.getPath());

				btnLoad.setEnabled(false);
				btnShow.setEnabled(true);
				btnCalculate.setEnabled(true);
				btnReset.setEnabled(true);

				showMessage("The log file " + file.getName() + " has been loaded.");
			} catch (CustomerException e) {
				showErrorMessage("Log file contains invalid customer details.");
			} catch (PizzaException e) {
				showErrorMessage("Log file contains invalid pizza details.");
			} catch (LogHandlerException e) {
				showErrorMessage("Log file contains semantic errors.");
			}
		} else if (retVal == JFileChooser.CANCEL_OPTION) {
			return; // Let cancels go without a message
		} else {
			showErrorMessage("There was an error loading the log file.");
		}
	}

	/**
	 * Handler for when the show button is pressed.
	 */
	private void btnShowHandler() {
		// Show Customers
		int numCustomers = restaurant.getNumCustomerOrders();
		for (int i = 0; i < numCustomers; i++) {
			try {
				mdlCustomers.add(restaurant.getCustomerByIndex(i));
			} catch (CustomerException e) {
				// This really shouldn't happen.
				showErrorMessage("There was an invalid index error when populating the customers table.");
			}
		}
		mdlCustomers.fireTableDataChanged();

		// Show Pizzas
		int numPizzas = restaurant.getNumPizzaOrders();
		for (int i = 0; i < numPizzas; i++) {
			try {
				mdlPizzas.add(restaurant.getPizzaByIndex(i));
			} catch (PizzaException e) {
				// This really shouldn't happen.
				showErrorMessage("There was an invalid index error when populating the orders table.");
			}
		}
		mdlPizzas.fireTableDataChanged();

		showMessage("The log data has been displayed in the tables.");

		btnShow.setEnabled(false);
	}

	/**
	 * Handler for when the calculate button is pressed.
	 */
	private void btnCalculateHandler() {
		// Total Distance
		pnlTotalDistance.setVisible(true);
		txfTotalDistance.setText(String.format("%.2f km", restaurant.getTotalDeliveryDistance()));

		// Total Profit
		pnlTotalProfit.setVisible(true);
		txfTotalProfit.setText(String.format("$%.2f", restaurant.getTotalProfit()));

		btnCalculate.setEnabled(false);

		showMessage("The total delivery distance and total profit has been calculated.");
	}

	/**
	 * Handler for when the reset button is pressed.
	 */
	private void btnResetHandler() {
		restaurant.resetDetails();
		resetTables();

		pnlTotalDistance.setVisible(false);
		pnlTotalProfit.setVisible(false);

		btnLoad.setEnabled(true);
		btnShow.setEnabled(false);
		btnCalculate.setEnabled(false);
		btnReset.setEnabled(false);

		showMessage("The log interpreter has been reset. Please load a new log file.");
	}

	/**
	 * Shows an info message to the user in a pop-up window.
	 * 
	 * @param message
	 *            - Message to show in pop-up window.
	 */
	private void showMessage(String message) {
		JOptionPane.showMessageDialog(this, message, "Pizza Palace", JOptionPane.INFORMATION_MESSAGE);
	}

	/**
	 * Shows a warning message to the user in a pop-up window.
	 * 
	 * @param message
	 *            - Message to show in error window.
	 */
	private void showErrorMessage(String message) {
		JOptionPane.showMessageDialog(this, message, "Pizza Palace - Error", JOptionPane.WARNING_MESSAGE);
	}

	@Override
	public void run() {

	}
}
