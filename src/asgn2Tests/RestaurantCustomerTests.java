package asgn2Tests;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import asgn2Exceptions.*;
import asgn2Customers.*;
import asgn2Restaurant.PizzaRestaurant;

/**
 * A class that that tests the methods relating to the handling of Customer
 * objects in the asgn2Restaurant.PizzaRestaurant class as well as processLog
 * and resetDetails.
 * 
 * @author Jason Chang
 */
public class RestaurantCustomerTests {
	private static final double DELTA = 1e-15; // DELTA for asserting doubles

	private PizzaRestaurant testRestaurant;

	@Before
	public void setupTestRestaurant() {
		testRestaurant = new PizzaRestaurant();
	}

	@Test
	public void numCustomerOrdersInitial() {
		assertEquals(testRestaurant.getNumCustomerOrders(), 0);
	}

	@Test
	public void numCustomerOrdersReset() throws PizzaException, LogHandlerException, CustomerException {
		testRestaurant.processLog(".//logs/20170101.txt");
		testRestaurant.resetDetails();
		assertEquals(testRestaurant.getNumCustomerOrders(), 0);

	}

	@Test
	public void numCustomerOrder1() throws PizzaException, LogHandlerException, CustomerException {
		testRestaurant.processLog(".//logs/20170101.txt");
		assertEquals(testRestaurant.getNumCustomerOrders(), 3);
	}

	@Test
	public void numCustomerOrders2() throws CustomerException, PizzaException, LogHandlerException {
		testRestaurant.processLog(".//logs/20170102.txt");
		assertEquals(testRestaurant.getNumCustomerOrders(), 10);
	}

	@Test
	public void numCustomerOrders3() throws CustomerException, PizzaException, LogHandlerException {
		testRestaurant.processLog(".//logs/20170103.txt");
		assertEquals(testRestaurant.getNumCustomerOrders(), 100);
	}

	@Test(expected = CustomerException.class)
	public void getCustomerByIndexInitial() throws CustomerException {
		testRestaurant.getCustomerByIndex(0);
	}

	@Test(expected = CustomerException.class)
	public void getCustomerByIndexReset() throws CustomerException, PizzaException, LogHandlerException {
		testRestaurant.processLog(".//logs/20170101.txt");
		testRestaurant.resetDetails();
		testRestaurant.getCustomerByIndex(0);
	}

	@Test
	public void getPizzaByIndex1() throws CustomerException, PizzaException, LogHandlerException {
		testRestaurant.processLog(".//logs/20170102.txt");
		DriverDeliveryCustomer expectedCustomer = new DriverDeliveryCustomer("Emma Brown", "0602547760", -1, 0);
		assertEquals(testRestaurant.getCustomerByIndex(0), expectedCustomer);
	}

	@Test
	public void getPizzaByIndex2() throws CustomerException, PizzaException, LogHandlerException {
		testRestaurant.processLog(".//logs/20170102.txt");
		PickUpCustomer expectedCustomer = new PickUpCustomer("Eli Wang", "0858312357", 0, 0);
		assertEquals(testRestaurant.getCustomerByIndex(5), expectedCustomer);
	}

	@Test
	public void getPizzaByIndex3() throws CustomerException, PizzaException, LogHandlerException {
		testRestaurant.processLog(".//logs/20170102.txt");
		DroneDeliveryCustomer expectedCustomer = new DroneDeliveryCustomer("Emma Chen", "0678585543", -4, 2);
		assertEquals(testRestaurant.getCustomerByIndex(7), expectedCustomer);
	}

	@Test(expected = CustomerException.class)
	public void getCustomerIndexTooLow() throws CustomerException {
		testRestaurant.getCustomerByIndex(-1);
	}

	@Test(expected = CustomerException.class)
	public void getCustomerIndexTooHigh() throws CustomerException, PizzaException, LogHandlerException {
		testRestaurant.processLog(".//logs/20170102.txt");
		testRestaurant.getCustomerByIndex(10);
	}

	@Test
	public void getTotalDeliveryDistanceInitial() {
		assertEquals(testRestaurant.getTotalDeliveryDistance(), 0.0, DELTA);
	}

	@Test
	public void getTotalDeliveryDistanceReset() throws CustomerException, PizzaException, LogHandlerException {
		testRestaurant.processLog(".//logs/20170101.txt");
		testRestaurant.resetDetails();
		assertEquals(testRestaurant.getTotalDeliveryDistance(), 0.0, DELTA);
	}

	@Test
	public void getTotalDeliveryDistance1() throws CustomerException, PizzaException, LogHandlerException {
		testRestaurant.processLog(".//logs/20170101.txt");
		assertEquals(testRestaurant.getTotalDeliveryDistance(), 15.0, DELTA);
	}

	@Test // Dan check this not sure how many decimal places I am meant to go to
	public void getTotalDeliveryDistance2() throws CustomerException, PizzaException, LogHandlerException {
		testRestaurant.processLog(".//logs/20170102.txt");
		assertEquals(testRestaurant.getTotalDeliveryDistance(), 41.41, 0.005);
	}

}
