package asgn2Tests;

import static org.junit.Assert.*;

import org.junit.Test;

import asgn2Exceptions.CustomerException;
import asgn2Customers.*;

/**
 * A class the that tests the asgn2Customers.CustomerFactory class.
 * 
 * @author Jason Chang
 *
 */
public class CustomerFactoryTests {

	// -------- Testing Type Input --------//

	@Test
	public void Customer_Pickup() throws CustomerException {
		PickUpCustomer expectedCustomer = new PickUpCustomer("TestName", "0411111111", 0, 0);
		Customer CustomerOutput = CustomerFactory.getCustomer("PUC", "TestName", "0411111111", 0, 0);
		assertEquals(expectedCustomer, CustomerOutput);
	}

	@Test
	public void Customer_Drone() throws CustomerException {
		DroneDeliveryCustomer expectedCustomer = new DroneDeliveryCustomer("TestName", "0411111111", 1, 1);
		Customer CustomerOutput = CustomerFactory.getCustomer("DNC", "TestName", "0411111111", 1, 1);
		assertEquals(expectedCustomer, CustomerOutput);
	}

	@Test
	public void Customer_Driver() throws CustomerException {
		DriverDeliveryCustomer expectedCustomer = new DriverDeliveryCustomer("TestName", "0411111111", 1, 1);
		Customer CustomerOutput = CustomerFactory.getCustomer("DVC", "TestName", "0411111111", 1, 1);
		assertEquals(expectedCustomer, CustomerOutput);
	}

	@Test(expected = CustomerException.class)
	public void InvalidCustomerCode_1() throws CustomerException {
		CustomerFactory.getCustomer("ABC", "TestName", "0411111111", 1, 1);
	}

	@Test(expected = CustomerException.class)
	public void InvalidCustomerCode_2() throws CustomerException {
		CustomerFactory.getCustomer("123", "TestName", "0411111111", 1, 1);
	}

	@Test(expected = CustomerException.class)
	public void InvalidCustomerCode_3() throws CustomerException {
		CustomerFactory.getCustomer("", "TestName", "0411111111", 1, 1);
	}

	@Test(expected = CustomerException.class)
	public void InvalidCustomerCode_4() throws CustomerException {
		CustomerFactory.getCustomer("!_()?$#@", "TestName", "0411111111", 1, 1);
	}

	// -------- Testing Different Customers --------//

	@Test
	public void Different_Code() throws CustomerException {
		PickUpCustomer expectedCustomer = new PickUpCustomer("TestName", "0411111111", 0, 0);
		Customer CustomerOutput = CustomerFactory.getCustomer("DNC", "TestName", "0411111111", 5, 0);
		assertNotEquals(expectedCustomer, CustomerOutput);
	}

	@Test
	public void Different_Name() throws CustomerException {
		PickUpCustomer expectedCustomer = new PickUpCustomer("TestName", "0411111111", 0, 0);
		Customer CustomerOutput = CustomerFactory.getCustomer("PUC", "Bob", "0411111111", 0, 0);
		assertNotEquals(expectedCustomer, CustomerOutput);
	}

	@Test
	public void Different_PhoneNumber() throws CustomerException {
		PickUpCustomer expectedCustomer = new PickUpCustomer("TestName", "0400000000", 0, 0);
		Customer CustomerOutput = CustomerFactory.getCustomer("PUC", "TestName", "0411111111", 0, 0);
		assertNotEquals(expectedCustomer, CustomerOutput);
	}

	@Test
	public void Different_Location() throws CustomerException {
		DriverDeliveryCustomer expectedCustomer = new DriverDeliveryCustomer("TestName", "0411111111", 5, 4);
		Customer CustomerOutput = CustomerFactory.getCustomer("DVC", "TestName", "0411111111", 1, 1);
		assertNotEquals(expectedCustomer, CustomerOutput);
	}

	@Test
	public void Different_Location_x() throws CustomerException {
		DriverDeliveryCustomer expectedCustomer = new DriverDeliveryCustomer("TestName", "0411111111", 5, 1);
		Customer CustomerOutput = CustomerFactory.getCustomer("DVC", "TestName", "0411111111", 1, 1);
		assertNotEquals(expectedCustomer, CustomerOutput);
	}

	@Test
	public void Different_Location_y() throws CustomerException {
		DriverDeliveryCustomer expectedCustomer = new DriverDeliveryCustomer("TestName", "0411111111", 1, 5);
		Customer CustomerOutput = CustomerFactory.getCustomer("DVC", "TestName", "0411111111", 1, 1);
		assertNotEquals(expectedCustomer, CustomerOutput);
	}
}
