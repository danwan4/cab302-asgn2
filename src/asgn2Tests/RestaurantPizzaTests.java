package asgn2Tests;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import java.time.LocalTime;

import asgn2Exceptions.*;
import asgn2Pizzas.*;
import asgn2Restaurant.PizzaRestaurant;

/**
 * A class that tests the methods relating to the handling of Pizza objects in
 * the asgn2Restaurant.PizzaRestaurant class as well as processLog and
 * resetDetails.
 * 
 * @author Daniel Wan
 *
 */

public class RestaurantPizzaTests {

	private static final double DELTA = 1e-15; // Delta for asserting doubles

	private PizzaRestaurant testRestaurant;

	@Before
	public void setupTestRestaurant() {
		testRestaurant = new PizzaRestaurant();
	}

	@Test
	public void processLog1() throws CustomerException, PizzaException, LogHandlerException {
		testRestaurant.processLog(".//logs/20170101.txt");
	}

	@Test
	public void processLog2() throws CustomerException, PizzaException, LogHandlerException {
		testRestaurant.processLog(".//logs/20170102.txt");
	}

	@Test
	public void processLog3() throws CustomerException, PizzaException, LogHandlerException {
		testRestaurant.processLog(".//logs/20170103.txt");
	}

	@Test(expected = LogHandlerException.class)
	public void invalidLog1() throws CustomerException, PizzaException, LogHandlerException {
		testRestaurant.processLog("");
	}

	@Test(expected = LogHandlerException.class)
	public void invalidLog2() throws CustomerException, PizzaException, LogHandlerException {
		testRestaurant.processLog("this_doesnt_exist.txt");
	}

	@Test
	public void numPizzaOrdersInitial() {
		assertEquals(testRestaurant.getNumPizzaOrders(), 0);
	}

	@Test
	public void numPizzaOrdersReset() throws CustomerException, PizzaException, LogHandlerException {
		processLog1();
		testRestaurant.resetDetails();
		assertEquals(testRestaurant.getNumPizzaOrders(), 0);
	}

	@Test
	public void numPizzaOrders1() throws CustomerException, PizzaException, LogHandlerException {
		processLog1();
		assertEquals(testRestaurant.getNumPizzaOrders(), 3);
	}

	@Test
	public void numPizzaOrders2() throws CustomerException, PizzaException, LogHandlerException {
		processLog2();
		assertEquals(testRestaurant.getNumPizzaOrders(), 10);
	}

	@Test
	public void numPizzaOrders3() throws CustomerException, PizzaException, LogHandlerException {
		processLog3();
		assertEquals(testRestaurant.getNumPizzaOrders(), 100);
	}

	@Test(expected = PizzaException.class)
	public void getPizzaByIndexInitial() throws PizzaException {
		testRestaurant.getPizzaByIndex(0);
	}

	@Test(expected = PizzaException.class)
	public void getPizzaByIndexReset() throws CustomerException, PizzaException, LogHandlerException {
		processLog1();
		testRestaurant.resetDetails();
		testRestaurant.getPizzaByIndex(0);
	}

	@Test
	public void getPizzaByIndex1() throws CustomerException, PizzaException, LogHandlerException {
		processLog2();
		VegetarianPizza expectedPizza = new VegetarianPizza(5, LocalTime.of(21, 17), LocalTime.of(21, 27));
		assertEquals(testRestaurant.getPizzaByIndex(0), expectedPizza);
	}

	@Test
	public void getPizzaByIndex2() throws CustomerException, PizzaException, LogHandlerException {
		processLog2();
		MeatLoversPizza expectedPizza = new MeatLoversPizza(7, LocalTime.of(21, 45), LocalTime.of(21, 56));
		assertEquals(testRestaurant.getPizzaByIndex(5), expectedPizza);
	}

	@Test
	public void getPizzaByIndex3() throws CustomerException, PizzaException, LogHandlerException {
		processLog2();
		MeatLoversPizza expectedPizza = new MeatLoversPizza(9, LocalTime.of(20, 47), LocalTime.of(21, 11));
		assertEquals(testRestaurant.getPizzaByIndex(9), expectedPizza);
	}

	@Test
	public void getPizzaByIndex4() throws CustomerException, PizzaException, LogHandlerException {
		processLog2();
		MargheritaPizza expectedPizza = new MargheritaPizza(1, LocalTime.of(21, 05), LocalTime.of(21, 34));
		assertEquals(testRestaurant.getPizzaByIndex(3), expectedPizza);
	}

	@Test(expected = PizzaException.class)
	public void getPizzaByInvalidIndex1() throws CustomerException, PizzaException, LogHandlerException {
		processLog2();
		testRestaurant.getPizzaByIndex(-1);
	}

	@Test(expected = PizzaException.class)
	public void getPizzaByInvalidIndex2() throws CustomerException, PizzaException, LogHandlerException {
		processLog2();
		testRestaurant.getPizzaByIndex(11);
	}

	@Test(expected = PizzaException.class)
	public void getPizzaIndexTooLow() throws PizzaException {
		testRestaurant.getPizzaByIndex(-1);
	}

	@Test(expected = PizzaException.class)
	public void getPizzaIndexTooHigh() throws CustomerException, PizzaException, LogHandlerException {
		processLog2();
		testRestaurant.getPizzaByIndex(10);
	}

	@Test
	public void getTotalProfitInitial() {
		assertEquals(testRestaurant.getTotalProfit(), 0.0, DELTA);
	}

	@Test
	public void getTotalProfitReset() throws CustomerException, PizzaException, LogHandlerException {
		processLog1();
		testRestaurant.resetDetails();
		assertEquals(testRestaurant.getTotalProfit(), 0.0, DELTA);
	}

	@Test
	public void getTotalProfit1() throws CustomerException, PizzaException, LogHandlerException {
		processLog1();
		assertEquals(testRestaurant.getTotalProfit(), 36.5, DELTA);
	}

	@Test
	public void getTotalProfit2() throws CustomerException, PizzaException, LogHandlerException {
		processLog2();
		assertEquals(testRestaurant.getTotalProfit(), 316.5, DELTA);
	}
}
