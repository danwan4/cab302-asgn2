package asgn2Tests;

import static org.junit.Assert.*;

import org.junit.Test;

import java.time.LocalTime;

import asgn2Exceptions.PizzaException;
import asgn2Pizzas.*;

/**
 * A class that that tests the asgn2Pizzas.MargheritaPizza,
 * asgn2Pizzas.VegetarianPizza, asgn2Pizzas.MeatLoversPizza classes. Note that
 * an instance of asgn2Pizzas.MeatLoversPizza should be used to test the
 * functionality of the asgn2Pizzas.Pizza abstract class.
 * 
 * @author Daniel Wan
 *
 */

public class PizzaTests {

	private static final double DELTA = 1e-15; // Delta for asserting doubles

	private static final int MINIMUM_QUANTITY = 1;
	private static final int MAXIMUM_QUANTITY = 10;

	/**
	 * Asserts the consistent properties of a meatlovers pizza.
	 * 
	 * @param pizza
	 *            - Pizza object to be checked.
	 */
	private void assertMeatLovers(MeatLoversPizza pizza) {
		assertEquals(pizza.getPizzaType(), "Meat Lovers");

		assertTrue(pizza.containsTopping(PizzaTopping.CHEESE));
		assertTrue(pizza.containsTopping(PizzaTopping.TOMATO));
		assertTrue(pizza.containsTopping(PizzaTopping.BACON));
		assertTrue(pizza.containsTopping(PizzaTopping.SALAMI));
		assertTrue(pizza.containsTopping(PizzaTopping.PEPPERONI));
		assertFalse(pizza.containsTopping(PizzaTopping.CAPSICUM));
		assertFalse(pizza.containsTopping(PizzaTopping.MUSHROOM));
		assertFalse(pizza.containsTopping(PizzaTopping.EGGPLANT));

		assertEquals(pizza.getPricePerPizza(), 12.0, DELTA);
	}

	/**
	 * Asserts the consistent properties of a margherita pizza.
	 * 
	 * @param pizza
	 *            - Pizza object to be checked.
	 */
	private void assertMargherita(MargheritaPizza pizza) {
		assertEquals(pizza.getPizzaType(), "Margherita");

		assertTrue(pizza.containsTopping(PizzaTopping.CHEESE));
		assertTrue(pizza.containsTopping(PizzaTopping.TOMATO));
		assertFalse(pizza.containsTopping(PizzaTopping.BACON));
		assertFalse(pizza.containsTopping(PizzaTopping.SALAMI));
		assertFalse(pizza.containsTopping(PizzaTopping.PEPPERONI));
		assertFalse(pizza.containsTopping(PizzaTopping.CAPSICUM));
		assertFalse(pizza.containsTopping(PizzaTopping.MUSHROOM));
		assertFalse(pizza.containsTopping(PizzaTopping.EGGPLANT));

		assertEquals(pizza.getPricePerPizza(), 8.0, DELTA);
	}

	/**
	 * Asserts the consistent properties of a vegetarian pizza.
	 * 
	 * @param pizza
	 *            - Pizza object to be checked.
	 */
	private void assertVegetarian(VegetarianPizza pizza) {
		assertEquals(pizza.getPizzaType(), "Vegetarian");

		assertTrue(pizza.containsTopping(PizzaTopping.CHEESE));
		assertTrue(pizza.containsTopping(PizzaTopping.TOMATO));
		assertFalse(pizza.containsTopping(PizzaTopping.BACON));
		assertFalse(pizza.containsTopping(PizzaTopping.SALAMI));
		assertFalse(pizza.containsTopping(PizzaTopping.PEPPERONI));
		assertTrue(pizza.containsTopping(PizzaTopping.CAPSICUM));
		assertTrue(pizza.containsTopping(PizzaTopping.MUSHROOM));
		assertTrue(pizza.containsTopping(PizzaTopping.EGGPLANT));

		assertEquals(pizza.getPricePerPizza(), 10.0, DELTA);
	}

	/* Pizza Tests */

	@SuppressWarnings("unused")
	@Test(expected = PizzaException.class)
	public void quantityTooLow() throws PizzaException {
		MeatLoversPizza testPizza = new MeatLoversPizza(MINIMUM_QUANTITY - 1, LocalTime.of(19, 0), LocalTime.of(19, 30));
	}

	@Test
	public void quantityLowest() throws PizzaException {
		MeatLoversPizza testPizza = new MeatLoversPizza(MINIMUM_QUANTITY, LocalTime.of(19, 0), LocalTime.of(19, 30));
		assertEquals(testPizza.getQuantity(), MINIMUM_QUANTITY);
	}

	@SuppressWarnings("unused")
	@Test(expected = PizzaException.class)
	public void quantityTooHigh() throws PizzaException {
		MeatLoversPizza testPizza = new MeatLoversPizza(MAXIMUM_QUANTITY + 1, LocalTime.of(19, 0), LocalTime.of(19, 30));
	}

	@Test
	public void quantityHighest() throws PizzaException {
		MeatLoversPizza testPizza = new MeatLoversPizza(MAXIMUM_QUANTITY, LocalTime.of(19, 0), LocalTime.of(19, 30));
		assertEquals(testPizza.getQuantity(), MAXIMUM_QUANTITY);
	}

	@SuppressWarnings("unused")
	@Test(expected = PizzaException.class)
	// Can order pizzas starting at 7PM
	public void orderTooEarly() throws PizzaException {
		MeatLoversPizza testPizza = new MeatLoversPizza(5, LocalTime.of(18, 59, 59, 999999999), LocalTime.of(19, 30));
	}

	@SuppressWarnings("unused")
	@Test
	// Can order pizzas starting at 7PM
	public void orderEarliest() throws PizzaException {
		MeatLoversPizza testPizza = new MeatLoversPizza(5, LocalTime.of(19, 0), LocalTime.of(19, 30));
	}

	@SuppressWarnings("unused")
	@Test(expected = PizzaException.class)
	// Can order pizzas until 11PM
	public void orderTooLate() throws PizzaException {
		MeatLoversPizza testPizza = new MeatLoversPizza(5, LocalTime.of(23, 0, 0, 1), LocalTime.of(23, 30));
	}

	@SuppressWarnings("unused")
	@Test
	// Can order pizzas until 11PM
	public void orderLatest() throws PizzaException {
		MeatLoversPizza testPizza = new MeatLoversPizza(5, LocalTime.of(23, 0), LocalTime.of(23, 30));
	}

	@SuppressWarnings("unused")
	@Test(expected = PizzaException.class)
	// Can't deliver pizza before it's ordered
	public void deliveryEarly1() throws PizzaException {
		MeatLoversPizza testPizza = new MeatLoversPizza(5, LocalTime.of(20, 0), LocalTime.of(19, 30));
	}

	@SuppressWarnings("unused")
	@Test(expected = PizzaException.class)
	// Pizzas require 10 minutes to be cooked
	public void deliveryEarly2() throws PizzaException {
		MeatLoversPizza testPizza = new MeatLoversPizza(5, LocalTime.of(20, 0), LocalTime.of(20, 9, 9, 999999999));
	}

	@SuppressWarnings("unused")
	@Test
	// Pizzas require 10 minutes to be cooked
	public void deliveryEarliest() throws PizzaException {
		MeatLoversPizza testPizza = new MeatLoversPizza(5, LocalTime.of(20, 0), LocalTime.of(20, 10));
	}

	@SuppressWarnings("unused")
	@Test(expected = PizzaException.class)
	// Pizza gets thrown out after 1 hour
	public void deliveryTooLate() throws PizzaException {
		MeatLoversPizza testPizza = new MeatLoversPizza(5, LocalTime.of(20, 0), LocalTime.of(21, 0, 0, 1));
	}

	@SuppressWarnings("unused")
	@Test
	// Pizza gets thrown out after 1 hour
	public void deliveryLatest() throws PizzaException {
		MeatLoversPizza testPizza = new MeatLoversPizza(5, LocalTime.of(20, 0), LocalTime.of(21, 0));
	}

	@SuppressWarnings("unused")
	@Test
	// Pizzas can be delivered after 11PM
	public void lateDelivery() throws PizzaException {
		MeatLoversPizza testPizza = new MeatLoversPizza(5, LocalTime.of(22, 45), LocalTime.of(23, 30));
	}

	@Test
	public void equals1() throws PizzaException {
		MeatLoversPizza testPizzaA = new MeatLoversPizza(5, LocalTime.of(19, 0), LocalTime.of(19, 30));
		MeatLoversPizza testPizzaB = new MeatLoversPizza(5, LocalTime.of(19, 0), LocalTime.of(19, 30));
		// Both were just constructed
		assertEquals(testPizzaA, testPizzaB);
	}

	@Test
	public void equals2() throws PizzaException {
		MeatLoversPizza testPizzaA = new MeatLoversPizza(10, LocalTime.of(19, 0), LocalTime.of(19, 30));
		MeatLoversPizza testPizzaB = new MeatLoversPizza(5, LocalTime.of(19, 0), LocalTime.of(19, 30));
		// Different quantities
		assertNotEquals(testPizzaA, testPizzaB);
	}

	@Test
	public void equals3() throws PizzaException {
		MeatLoversPizza testPizzaA = new MeatLoversPizza(5, LocalTime.of(19, 0), LocalTime.of(19, 30));
		MeatLoversPizza testPizzaB = new MeatLoversPizza(5, LocalTime.of(19, 0), LocalTime.of(19, 30));
		testPizzaA.calculateCostPerPizza();
		// Only one has cost calculated
		assertNotEquals(testPizzaA, testPizzaB);
	}

	@Test
	public void equals4() throws PizzaException {
		MeatLoversPizza testPizzaA = new MeatLoversPizza(10, LocalTime.of(19, 0), LocalTime.of(19, 30));
		MeatLoversPizza testPizzaB = new MeatLoversPizza(10, LocalTime.of(19, 0), LocalTime.of(19, 30));
		testPizzaA.calculateCostPerPizza();
		testPizzaB.calculateCostPerPizza();
		// Both have cost calculated
		assertEquals(testPizzaA, testPizzaB);
	}

	@Test
	public void equals5() throws PizzaException {
		MeatLoversPizza testPizzaA = new MeatLoversPizza(5, LocalTime.of(19, 0), LocalTime.of(19, 30));
		MeatLoversPizza testPizzaB = new MeatLoversPizza(10, LocalTime.of(19, 0), LocalTime.of(19, 30));
		testPizzaA.calculateCostPerPizza();
		testPizzaB.calculateCostPerPizza();
		// Both have cost calculated, but different quantities
		assertNotEquals(testPizzaA, testPizzaB);
	}

	@Test
	public void various1() throws PizzaException {
		// Do various things in various order
		// i.e. check doing methods doesn't affect other methods
		MeatLoversPizza testPizza = new MeatLoversPizza(2, LocalTime.of(20, 30), LocalTime.of(21, 0));

		testPizza.calculateCostPerPizza();

		assertMeatLovers(testPizza);
		assertEquals(testPizza.getQuantity(), 2);

		assertEquals(testPizza.getCostPerPizza(), 5.0, DELTA);
		assertEquals(testPizza.getPricePerPizza(), 12.0, DELTA);
		assertEquals(testPizza.getOrderCost(), 10.0, DELTA);
		assertEquals(testPizza.getOrderPrice(), 24.0, DELTA);
		assertEquals(testPizza.getOrderProfit(), 14.0, DELTA);

		assertMeatLovers(testPizza);
		assertEquals(testPizza.getQuantity(), 2);

		assertEquals(testPizza.getCostPerPizza(), 5.0, DELTA);
		assertEquals(testPizza.getPricePerPizza(), 12.0, DELTA);
		assertEquals(testPizza.getOrderCost(), 10.0, DELTA);
		assertEquals(testPizza.getOrderPrice(), 24.0, DELTA);
		assertEquals(testPizza.getOrderProfit(), 14.0, DELTA);
	}

	@Test
	public void various2() throws PizzaException {
		// Do various things in various order
		// i.e. check doing methods doesn't affect other methods
		MeatLoversPizza testPizza = new MeatLoversPizza(3, LocalTime.of(20, 30), LocalTime.of(21, 0));

		testPizza.calculateCostPerPizza();
		testPizza.calculateCostPerPizza();
		testPizza.calculateCostPerPizza();

		assertEquals(testPizza.getCostPerPizza(), 5.0, DELTA);
		assertEquals(testPizza.getPricePerPizza(), 12.0, DELTA);
		assertEquals(testPizza.getOrderCost(), 15.0, DELTA);
		assertEquals(testPizza.getOrderPrice(), 36.0, DELTA);
		assertEquals(testPizza.getOrderProfit(), 21.0, DELTA);

		testPizza.calculateCostPerPizza();

		assertMeatLovers(testPizza);
		assertEquals(testPizza.getQuantity(), 3);

		assertEquals(testPizza.getCostPerPizza(), 5.0, DELTA);
		assertEquals(testPizza.getPricePerPizza(), 12.0, DELTA);
		assertEquals(testPizza.getOrderCost(), 15.0, DELTA);
		assertEquals(testPizza.getOrderPrice(), 36.0, DELTA);
		assertEquals(testPizza.getOrderProfit(), 21.0, DELTA);
	}

	/* MeatLoversPizza Tests */

	@Test
	public void mlConstructor1() throws PizzaException {
		MeatLoversPizza testPizza = new MeatLoversPizza(5, LocalTime.of(19, 0), LocalTime.of(19, 30));
		assertMeatLovers(testPizza);
		assertEquals(testPizza.getQuantity(), 5);
		// Constructor should not set cost per pizza
		assertEquals(testPizza.getCostPerPizza(), 0.0, DELTA);
		assertEquals(testPizza.getPricePerPizza(), 12.0, DELTA);
		assertEquals(testPizza.getOrderCost(), 0.0, DELTA);
		assertEquals(testPizza.getOrderPrice(), 60.0, DELTA);
		assertEquals(testPizza.getOrderProfit(), 60.0, DELTA);
	}

	@Test
	public void mlConstructor2() throws PizzaException {
		MeatLoversPizza testPizza = new MeatLoversPizza(7, LocalTime.of(21, 0), LocalTime.of(21, 30));
		assertMeatLovers(testPizza);
		assertEquals(testPizza.getQuantity(), 7);
		// Constructor should not set cost per pizza
		assertEquals(testPizza.getCostPerPizza(), 0.0, DELTA);
		assertEquals(testPizza.getPricePerPizza(), 12.0, DELTA);
		assertEquals(testPizza.getOrderCost(), 0.0, DELTA);
		assertEquals(testPizza.getOrderPrice(), 84.0, DELTA);
		assertEquals(testPizza.getOrderProfit(), 84.0, DELTA);
	}

	@Test
	public void mlCost1() throws PizzaException {
		MeatLoversPizza testPizza = new MeatLoversPizza(1, LocalTime.of(20, 30), LocalTime.of(21, 0));
		testPizza.calculateCostPerPizza();
		assertEquals(testPizza.getCostPerPizza(), 5.0, DELTA);
		assertEquals(testPizza.getPricePerPizza(), 12.0, DELTA);
		assertEquals(testPizza.getOrderCost(), 5.0, DELTA);
		assertEquals(testPizza.getOrderPrice(), 12.0, DELTA);
		assertEquals(testPizza.getOrderProfit(), 7.0, DELTA);
	}

	@Test
	public void mlCost2() throws PizzaException {
		MeatLoversPizza testPizza = new MeatLoversPizza(5, LocalTime.of(20, 30), LocalTime.of(21, 0));
		testPizza.calculateCostPerPizza();
		assertEquals(testPizza.getCostPerPizza(), 5.0, DELTA);
		assertEquals(testPizza.getPricePerPizza(), 12.0, DELTA);
		assertEquals(testPizza.getOrderCost(), 25.0, DELTA);
		assertEquals(testPizza.getOrderPrice(), 60.0, DELTA);
		assertEquals(testPizza.getOrderProfit(), 35.0, DELTA);
	}

	@Test
	public void mlCost3() throws PizzaException {
		MeatLoversPizza testPizza = new MeatLoversPizza(7, LocalTime.of(20, 30), LocalTime.of(21, 0));
		testPizza.calculateCostPerPizza();
		assertEquals(testPizza.getCostPerPizza(), 5.0, DELTA);
		assertEquals(testPizza.getPricePerPizza(), 12.0, DELTA);
		assertEquals(testPizza.getOrderCost(), 35.0, DELTA);
		assertEquals(testPizza.getOrderPrice(), 84.0, DELTA);
		assertEquals(testPizza.getOrderProfit(), 49.0, DELTA);
	}

	/* MargheritaPizza Tests */

	@Test
	public void margConstructor1() throws PizzaException {
		MargheritaPizza testPizza = new MargheritaPizza(3, LocalTime.of(19, 0), LocalTime.of(19, 30));
		assertMargherita(testPizza);
		assertEquals(testPizza.getQuantity(), 3);
		// Constructor should not set cost per pizza
		assertEquals(testPizza.getCostPerPizza(), 0.0, DELTA);
		assertEquals(testPizza.getPricePerPizza(), 8.0, DELTA);
		assertEquals(testPizza.getOrderCost(), 0.0, DELTA);
		assertEquals(testPizza.getOrderPrice(), 24.0, DELTA);
		assertEquals(testPizza.getOrderProfit(), 24.0, DELTA);
	}

	@Test
	public void margConstructor2() throws PizzaException {
		MargheritaPizza testPizza = new MargheritaPizza(5, LocalTime.of(21, 0), LocalTime.of(21, 30));
		assertMargherita(testPizza);
		assertEquals(testPizza.getQuantity(), 5);
		// Constructor should not set cost per pizza
		assertEquals(testPizza.getCostPerPizza(), 0.0, DELTA);
		assertEquals(testPizza.getPricePerPizza(), 8.0, DELTA);
		assertEquals(testPizza.getOrderCost(), 0.0, DELTA);
		assertEquals(testPizza.getOrderPrice(), 40.0, DELTA);
		assertEquals(testPizza.getOrderProfit(), 40.0, DELTA);
	}

	@Test
	public void margCost1() throws PizzaException {
		MargheritaPizza testPizza = new MargheritaPizza(1, LocalTime.of(19, 0), LocalTime.of(19, 30));
		testPizza.calculateCostPerPizza();
		assertEquals(testPizza.getCostPerPizza(), 1.5, DELTA);
		assertEquals(testPizza.getPricePerPizza(), 8.0, DELTA);
		assertEquals(testPizza.getOrderCost(), 1.5, DELTA);
		assertEquals(testPizza.getOrderPrice(), 8.0, DELTA);
		assertEquals(testPizza.getOrderProfit(), 6.5, DELTA);
	}

	@Test
	public void margCost2() throws PizzaException {
		MargheritaPizza testPizza = new MargheritaPizza(5, LocalTime.of(19, 0), LocalTime.of(19, 30));
		testPizza.calculateCostPerPizza();
		assertEquals(testPizza.getCostPerPizza(), 1.5, DELTA);
		assertEquals(testPizza.getPricePerPizza(), 8.0, DELTA);
		assertEquals(testPizza.getOrderCost(), 7.5, DELTA);
		assertEquals(testPizza.getOrderPrice(), 40.0, DELTA);
		assertEquals(testPizza.getOrderProfit(), 32.5, DELTA);
	}

	@Test
	public void margCost3() throws PizzaException {
		MargheritaPizza testPizza = new MargheritaPizza(7, LocalTime.of(19, 0), LocalTime.of(19, 30));
		testPizza.calculateCostPerPizza();
		assertEquals(testPizza.getCostPerPizza(), 1.5, DELTA);
		assertEquals(testPizza.getPricePerPizza(), 8.0, DELTA);
		assertEquals(testPizza.getOrderCost(), 10.5, DELTA);
		assertEquals(testPizza.getOrderPrice(), 56.0, DELTA);
		assertEquals(testPizza.getOrderProfit(), 45.5, DELTA);
	}

	/* VegetarianPizza Tests */

	@Test
	public void vegeConstructor1() throws PizzaException {
		VegetarianPizza testPizza = new VegetarianPizza(3, LocalTime.of(19, 0), LocalTime.of(19, 30));
		assertVegetarian(testPizza);
		assertEquals(testPizza.getQuantity(), 3);
		// Constructor should not set cost per pizza
		assertEquals(testPizza.getCostPerPizza(), 0.0, DELTA);
		assertEquals(testPizza.getPricePerPizza(), 10.0, DELTA);
		assertEquals(testPizza.getOrderCost(), 0.0, DELTA);
		assertEquals(testPizza.getOrderPrice(), 30.0, DELTA);
		assertEquals(testPizza.getOrderProfit(), 30.0, DELTA);
	}

	@Test
	public void vegeConstructor2() throws PizzaException {
		VegetarianPizza testPizza = new VegetarianPizza(5, LocalTime.of(21, 0), LocalTime.of(21, 30));
		assertVegetarian(testPizza);
		assertEquals(testPizza.getQuantity(), 5);
		// Constructor should not set cost per pizza
		assertEquals(testPizza.getCostPerPizza(), 0.0, DELTA);
		assertEquals(testPizza.getPricePerPizza(), 10.0, DELTA);
		assertEquals(testPizza.getOrderCost(), 0.0, DELTA);
		assertEquals(testPizza.getOrderPrice(), 50.0, DELTA);
		assertEquals(testPizza.getOrderProfit(), 50.0, DELTA);
	}

	@Test
	public void vegeCost1() throws PizzaException {
		VegetarianPizza testPizza = new VegetarianPizza(1, LocalTime.of(19, 0), LocalTime.of(19, 30));
		testPizza.calculateCostPerPizza();
		assertEquals(testPizza.getCostPerPizza(), 5.5, DELTA);
		assertEquals(testPizza.getPricePerPizza(), 10.0, DELTA);
		assertEquals(testPizza.getOrderCost(), 5.5, DELTA);
		assertEquals(testPizza.getOrderPrice(), 10.0, DELTA);
		assertEquals(testPizza.getOrderProfit(), 4.5, DELTA);
	}

	@Test
	public void vegeCost2() throws PizzaException {
		VegetarianPizza testPizza = new VegetarianPizza(5, LocalTime.of(19, 0), LocalTime.of(19, 30));
		testPizza.calculateCostPerPizza();
		assertEquals(testPizza.getCostPerPizza(), 5.5, DELTA);
		assertEquals(testPizza.getPricePerPizza(), 10.0, DELTA);
		assertEquals(testPizza.getOrderCost(), 27.5, DELTA);
		assertEquals(testPizza.getOrderPrice(), 50.0, DELTA);
		assertEquals(testPizza.getOrderProfit(), 22.5, DELTA);
	}

	@Test
	public void vegeCost3() throws PizzaException {
		VegetarianPizza testPizza = new VegetarianPizza(7, LocalTime.of(19, 0), LocalTime.of(19, 30));
		testPizza.calculateCostPerPizza();
		assertEquals(testPizza.getCostPerPizza(), 5.5, DELTA);
		assertEquals(testPizza.getPricePerPizza(), 10.0, DELTA);
		assertEquals(testPizza.getOrderCost(), 38.5, DELTA);
		assertEquals(testPizza.getOrderPrice(), 70.0, DELTA);
		assertEquals(testPizza.getOrderProfit(), 31.5, DELTA);
	}
}
