package asgn2Tests;

import static org.junit.Assert.*;

import java.time.LocalTime;

import java.util.ArrayList;

import org.junit.Test;

import asgn2Exceptions.*;
import asgn2Pizzas.*;
import asgn2Restaurant.LogHandler;

/**
 * A class that tests the methods relating to the creation of Pizza objects in
 * the asgn2Restaurant.LogHander class.
 * 
 * @author Daniel Wan
 * 
 */

public class LogHandlerPizzaTests {

	@Test
	public void populatePizzaDataset1() throws PizzaException, LogHandlerException {
		ArrayList<Pizza> pizzas = LogHandler.populatePizzaDataset(".//logs/20170101.txt");
		assertEquals(pizzas.size(), 3);
	}

	@Test
	public void populatePizzaDataset2() throws PizzaException, LogHandlerException {
		ArrayList<Pizza> pizzas = LogHandler.populatePizzaDataset(".//logs/20170102.txt");
		assertEquals(pizzas.size(), 10);
	}

	@Test
	public void populatePizzaDataset3() throws PizzaException, LogHandlerException {
		ArrayList<Pizza> pizzas = LogHandler.populatePizzaDataset(".//logs/20170103.txt");
		assertEquals(pizzas.size(), 100);
	}

	@Test
	public void datasetContent1() throws PizzaException, LogHandlerException {
		VegetarianPizza expectedPizza = new VegetarianPizza(2, LocalTime.of(19, 00), LocalTime.of(19, 20));
		ArrayList<Pizza> pizzas = LogHandler.populatePizzaDataset(".//logs/20170101.txt");
		assertEquals(expectedPizza, pizzas.get(0));
	}

	@Test
	public void datasetContent2() throws PizzaException, LogHandlerException {
		MargheritaPizza expectedPizza = new MargheritaPizza(1, LocalTime.of(20, 00), LocalTime.of(20, 25));
		ArrayList<Pizza> pizzas = LogHandler.populatePizzaDataset(".//logs/20170101.txt");
		assertEquals(expectedPizza, pizzas.get(1));
	}

	@Test
	public void datasetContent3() throws PizzaException, LogHandlerException {
		MeatLoversPizza expectedPizza = new MeatLoversPizza(3, LocalTime.of(21, 00), LocalTime.of(21, 35));
		ArrayList<Pizza> pizzas = LogHandler.populatePizzaDataset(".//logs/20170101.txt");
		assertEquals(expectedPizza, pizzas.get(2));
	}

	@Test
	public void datasetContent4() throws PizzaException, LogHandlerException {
		MeatLoversPizza expectedPizza = new MeatLoversPizza(7, LocalTime.of(20, 33), LocalTime.of(20, 54));
		ArrayList<Pizza> pizzas = LogHandler.populatePizzaDataset(".//logs/20170102.txt");
		assertEquals(expectedPizza, pizzas.get(5));
	}

	@Test
	public void datasetContent5() throws PizzaException, LogHandlerException {
		MargheritaPizza expectedPizza = new MargheritaPizza(5, LocalTime.of(21, 45), LocalTime.of(21, 56));
		ArrayList<Pizza> pizzas = LogHandler.populatePizzaDataset(".//logs/20170103.txt");
		assertEquals(expectedPizza, pizzas.get(99));
	}

	@Test
	public void createPizza1() throws PizzaException, LogHandlerException {
		MeatLoversPizza expectedPizza = new MeatLoversPizza(3, LocalTime.of(21, 00), LocalTime.of(21, 35));
		Pizza outputPizza = LogHandler.createPizza("21:00:00,21:35:00,Oroku Saki,0111222333,PUC,0,0,PZL,3");
		assertEquals(expectedPizza, outputPizza);
	}

	@Test
	public void createPizza2() throws PizzaException, LogHandlerException {
		MargheritaPizza expectedPizza = new MargheritaPizza(1, LocalTime.of(20, 00), LocalTime.of(20, 25));
		Pizza outputPizza = LogHandler.createPizza("20:00:00,20:25:00,April O'Neal,0987654321,DNC,3,4,PZM,1");
		assertEquals(expectedPizza, outputPizza);
	}

	@Test
	public void createPizza3() throws PizzaException, LogHandlerException {
		VegetarianPizza expectedPizza = new VegetarianPizza(2, LocalTime.of(19, 00), LocalTime.of(19, 20));
		Pizza outputPizza = LogHandler.createPizza("19:00:00,19:20:00,Casey Jones,0123456789,DVC,5,5,PZV,2");
		assertEquals(expectedPizza, outputPizza);
	}

	@Test(expected = LogHandlerException.class)
	public void emptyString() throws PizzaException, LogHandlerException {
		LogHandler.createPizza("");
	}

	@Test(expected = LogHandlerException.class)
	public void notEnoughValues() throws PizzaException, LogHandlerException {
		LogHandler.createPizza("19:00:00,19:20:00,Casey Jones,0123456789,DVC,5,5,PZV");
	}

	@Test(expected = LogHandlerException.class)
	public void tooManyValues() throws PizzaException, LogHandlerException {
		LogHandler.createPizza("19:00:00,19:20:00,Casey Jones,0123456789,DVC,5,5,PZV,2,6");
	}

	@Test(expected = LogHandlerException.class)
	public void invalidOrderTimeType1() throws PizzaException, LogHandlerException {
		LogHandler.createPizza("Casey Jones,19:20:00,Casey Jones,0123456789,DVC,5,5,PZV,2");
	}

	@Test(expected = LogHandlerException.class)
	public void invalidOrderTimeType2() throws PizzaException, LogHandlerException {
		LogHandler.createPizza("190000,19:20:00,Casey Jones,0123456789,DVC,5,5,PZV,2");
	}

	@Test(expected = LogHandlerException.class)
	public void invalidDeliveryTimeType1() throws PizzaException, LogHandlerException {
		LogHandler.createPizza("19:00:00,Casey Jones,Casey Jones,0123456789,DVC,5,5,PZV,2");
	}

	@Test(expected = LogHandlerException.class)
	public void invalidDeliveryTimeType2() throws PizzaException, LogHandlerException {
		LogHandler.createPizza("19:00:00,192000,Casey Jones,0123456789,DVC,5,5,PZV,2");
	}

	@Test(expected = LogHandlerException.class)
	public void invalidQuantityType() throws PizzaException, LogHandlerException {
		LogHandler.createPizza("19:00:00,19:20:00,Casey Jones,0123456789,DVC,5,5,10,Casey Jones");
	}

	@Test(expected = PizzaException.class)
	public void invalidPizzaType1() throws PizzaException, LogHandlerException {
		LogHandler.createPizza("19:00:00,19:20:00,Casey Jones,0123456789,DVC,5,5,10,2");
	}

	@Test(expected = PizzaException.class)
	public void invalidPizzaType2() throws PizzaException, LogHandlerException {
		LogHandler.createPizza("19:00:00,19:20:00,Casey Jones,0123456789,DVC,5,5,LUL,2");
	}

	@Test(expected = PizzaException.class)
	public void invalidPizzaQuantity() throws PizzaException, LogHandlerException {
		LogHandler.createPizza("19:00:00,19:20:00,Casey Jones,0123456789,DVC,5,5,PZV,15");
	}

	@Test(expected = PizzaException.class)
	public void invalidOrderTime1() throws PizzaException, LogHandlerException {
		LogHandler.createPizza("18:30:00,19:20:00,Casey Jones,0123456789,DVC,5,5,PZV,5");
	}

	@Test(expected = PizzaException.class)
	public void invalidOrderTime2() throws PizzaException, LogHandlerException {
		LogHandler.createPizza("23:10:00,23:30:00,Casey Jones,0123456789,DVC,5,5,PZV,5");
	}

	@Test(expected = PizzaException.class)
	public void invaidDeliveryTime1() throws PizzaException, LogHandlerException {
		LogHandler.createPizza("19:30:00,19:20:00,Casey Jones,0123456789,DVC,5,5,PZV,5");
	}

	@Test(expected = PizzaException.class)
	public void invaidDeliveryTime2() throws PizzaException, LogHandlerException {
		LogHandler.createPizza("19:30:00,21:00:00,Casey Jones,0123456789,DVC,5,5,PZV,5");
	}
}
