package asgn2Tests;

import static org.junit.Assert.*;

import org.junit.Test;

import asgn2Exceptions.CustomerException;
import asgn2Customers.*;

/**
 * A class that tests the that tests the asgn2Customers.PickUpCustomer,
 * asgn2Customers.DriverDeliveryCustomer, asgn2Customers.DroneDeliveryCustomer
 * classes. Note that an instance of asgn2Customers.DriverDeliveryCustomer
 * should be used to test the functionality of the asgn2Customers.Customer
 * abstract class.
 * 
 * @author Jason Chang
 * 
 */
public class CustomerTests {
	private static final double DELTA = 1e-15; // DELTA for asserting doubles

	private PickUpCustomer testPucCustomer1;
	private PickUpCustomer testPucCustomer2;
	private DroneDeliveryCustomer testDroneCustomer;
	private DriverDeliveryCustomer testDriverCustomer;

	/********* PickUpCustomer *********/

	// -------- Testing Constructor --------//

	@Test
	public void PucConstructor1() throws CustomerException {
		testPucCustomer1 = new PickUpCustomer("TestName", "0411111111", 0, 0);

		assertEquals(testPucCustomer1.getName(), "TestName");
		assertEquals(testPucCustomer1.getMobileNumber(), "0411111111");
		assertEquals(testPucCustomer1.getCustomerType(), "Pick Up");
		assertEquals(testPucCustomer1.getLocationX(), 0);
		assertEquals(testPucCustomer1.getLocationY(), 0);
		assertEquals(testPucCustomer1.getDeliveryDistance(), 0.0, DELTA);
	}

	@Test
	public void PucConstructor2() throws CustomerException {
		testPucCustomer2 = new PickUpCustomer("Bob", "0400000000", 0, 0);

		assertEquals(testPucCustomer2.getName(), "Bob");
		assertEquals(testPucCustomer2.getMobileNumber(), "0400000000");
		assertEquals(testPucCustomer2.getCustomerType(), "Pick Up");
		assertEquals(testPucCustomer2.getLocationX(), 0);
		assertEquals(testPucCustomer2.getLocationY(), 0);
		assertEquals(testPucCustomer2.getDeliveryDistance(), 0.0, DELTA);
	}

	@Test
	public void PucConstructor3() throws CustomerException {
		testPucCustomer2 = new PickUpCustomer("Daniel Wan", "0123456789", 0, 0);

		assertEquals(testPucCustomer2.getName(), "Daniel Wan");
		assertEquals(testPucCustomer2.getMobileNumber(), "0123456789");
		assertEquals(testPucCustomer2.getCustomerType(), "Pick Up");
		assertEquals(testPucCustomer2.getLocationX(), 0);
		assertEquals(testPucCustomer2.getLocationY(), 0);
		assertEquals(testPucCustomer2.getDeliveryDistance(), 0.0, DELTA);
	}

	@Test
	public void equals() throws CustomerException {
		testPucCustomer1 = new PickUpCustomer("TestName", "0411111111", 0, 0);
		testPucCustomer2 = new PickUpCustomer("TestName", "0411111111", 0, 0);
		assertTrue(testPucCustomer1.equals(testPucCustomer2));
	}

	// -------- Testing Name Input --------//

	@Test(expected = CustomerException.class)
	// Minimum 1 Character
	public void EmptyName() throws CustomerException {
		testPucCustomer1 = new PickUpCustomer("", "0400000000", 0, 0);
	}

	@Test
	// Minimum 1 Character
	public void Minimum_Name() throws CustomerException {
		testPucCustomer1 = new PickUpCustomer("A", "0400000000", 0, 0);
	}

	@Test
	// Maximum 20 Character
	public void Maximum_Name() throws CustomerException {
		testPucCustomer1 = new PickUpCustomer("AAAAAAAAAAAAAAAAAAAA", "0400000000", 0, 0);
	}

	@Test(expected = CustomerException.class)
	// Maximum 20 Character
	public void Over_Maximum_Name() throws CustomerException {
		testPucCustomer1 = new PickUpCustomer("AAAAAAAAAAAAAAAAAAAAA", "0400000000", 0, 0);
	}

	@Test
	// No White spaces only for name
	public void Whitespace_and_Character_Name() throws CustomerException {
		testPucCustomer1 = new PickUpCustomer("A B C D", "0400000000", 0, 0);
	}

	@Test(expected = CustomerException.class)
	// No White spaces only for name
	public void Whitespace_Name() throws CustomerException {
		testPucCustomer1 = new PickUpCustomer("     ", "0400000000", 0, 0);
	}

	@Test
	// Number Name (as string) wasn't stated in task sheet digits weren't
	// allowed
	public void Number_Name() throws CustomerException {
		testPucCustomer1 = new PickUpCustomer("1234", "0400000000", 0, 0);
	}

	@Test
	// Accepted Symbol (as string)
	public void Symbol_Name() throws CustomerException {
		testPucCustomer1 = new PickUpCustomer("Testing O'Test", "0400000000", 0, 0);
	}

	@Test
	// Unaccepted Symbols (as string)
	public void Unaccepted_Symbol_Name() throws CustomerException {
		testPucCustomer1 = new PickUpCustomer("!@#$%^&*()?><", "0400000000", 0, 0);
	}

	// -------- Testing Mobile Input --------//

	@Test
	// Valid phone number
	public void Valid_Number() throws CustomerException {
		testPucCustomer1 = new PickUpCustomer("TestName", "0123456789", 0, 0);
	}

	@Test(expected = CustomerException.class)
	// mobile number must be 10 digits
	public void Under_Ten_Digit_Number_1() throws CustomerException {
		testPucCustomer1 = new PickUpCustomer("TestName", "012345678", 0, 0);
	}

	@Test(expected = CustomerException.class)
	// mobile number must be 10 digits
	public void Under_Ten_Digit_Number_2() throws CustomerException {
		testPucCustomer1 = new PickUpCustomer("TestName", "01234", 0, 0);
	}

	@Test(expected = CustomerException.class)
	// mobile number must be 10 digits
	public void Under_Ten_Digit_Number_3() throws CustomerException {
		testPucCustomer1 = new PickUpCustomer("TestName", "0", 0, 0);
	}

	@Test(expected = CustomerException.class)
	// mobile number must be 10 digits
	public void Under_Ten_Digit_Number_4() throws CustomerException {
		testPucCustomer1 = new PickUpCustomer("TestName", "", 0, 0);
	}

	@Test(expected = CustomerException.class)
	// mobile number must be 10 digits
	public void Over_Ten_Digit_Number_1() throws CustomerException {
		testPucCustomer1 = new PickUpCustomer("TestName", "01234567890", 0, 0);
	}

	@Test(expected = CustomerException.class)
	// mobile number must be 10 digits
	public void Over_Ten_Digit_Number_2() throws CustomerException {
		testPucCustomer1 = new PickUpCustomer("TestName", "01234567890123456789", 0, 0);
	}

	@Test(expected = CustomerException.class)
	// mobile number must start with 0
	public void Non_0_start_1() throws CustomerException {
		testPucCustomer1 = new PickUpCustomer("TestName", "1234567890", 0, 0);
	}

	@Test(expected = CustomerException.class)
	// mobile number must start with 0
	public void Non_0_start_2() throws CustomerException {
		testPucCustomer1 = new PickUpCustomer("TestName", "2234567890", 0, 0);
	}

	@Test(expected = CustomerException.class)
	// mobile number must start with 0
	public void Non_0_start_3() throws CustomerException {
		testPucCustomer1 = new PickUpCustomer("TestName", "7234567890", 0, 0);
	}

	@Test(expected = CustomerException.class)
	// mobile number must start with 0
	public void Non_0_start_4() throws CustomerException {
		testPucCustomer1 = new PickUpCustomer("TestName", "9234567890", 0, 0);
	}

	@Test(expected = CustomerException.class)
	// mobile number must all be digits
	public void All_Non_Digit() throws CustomerException {
		testPucCustomer1 = new PickUpCustomer("TestName", "AAAAAAAAAA", 0, 0);
	}

	@Test(expected = CustomerException.class)
	// mobile number must all be digits
	public void Some_Non_Digit_1() throws CustomerException {
		testPucCustomer1 = new PickUpCustomer("TestName", "0AAAAAAAAA", 0, 0);
	}

	@Test(expected = CustomerException.class)
	// mobile number must all be digits
	public void Some_Non_Digit_2() throws CustomerException {
		testPucCustomer1 = new PickUpCustomer("TestName", "00AA0A0A00", 0, 0);
	}

	@Test(expected = CustomerException.class)
	// mobile number must all be digits
	public void Some_Non_Digit_3() throws CustomerException {
		testPucCustomer1 = new PickUpCustomer("TestName", "0=A!4A5A?9", 0, 0);
	}

	// -------- Testing Location --------//

	@Test
	// Pick Up Location must be 0,0
	public void Location_0_0() throws CustomerException {
		testPucCustomer1 = new PickUpCustomer("TestName", "0400000000", 0, 0);
	}

	@Test(expected = CustomerException.class)
	// Pick Up Location must be 0,0
	public void Location_not_valid_1() throws CustomerException {
		testPucCustomer1 = new PickUpCustomer("TestName", "0400000000", 1, 0);
	}

	@Test(expected = CustomerException.class)
	// Pick Up Location must be 0,0
	public void Location_not_valid_2() throws CustomerException {
		testPucCustomer1 = new PickUpCustomer("TestName", "0400000000", -1, 0);
	}

	@Test(expected = CustomerException.class)
	// Pick Up Location must be 0,0
	public void Location_not_valid_3() throws CustomerException {
		testPucCustomer1 = new PickUpCustomer("TestName", "0400000000", 0, 1);
	}

	@Test(expected = CustomerException.class)
	// Pick Up Location must be 0,0
	public void Location_not_valid_4() throws CustomerException {
		testPucCustomer1 = new PickUpCustomer("TestName", "0400000000", 0, -1);
	}

	@Test(expected = CustomerException.class)
	// Pick Up Location must be 0,0
	public void Location_not_valid_5() throws CustomerException {
		testPucCustomer1 = new PickUpCustomer("TestName", "0400000000", -5, 7);
	}

	@Test(expected = CustomerException.class)
	// Pick Up Location must be 0,0
	public void Location_not_valid_6() throws CustomerException {
		testPucCustomer1 = new PickUpCustomer("TestName", "0400000000", 25, -17);
	}

	/********* Drone Delivery Customer *********/

	// -------- Testing Constructor --------//

	@Test
	public void DroneConstructor() throws CustomerException {
		testDroneCustomer = new DroneDeliveryCustomer("TestName", "0411111111", 4, 3);

		assertEquals(testDroneCustomer.getName(), "TestName");
		assertEquals(testDroneCustomer.getMobileNumber(), "0411111111");
		assertEquals(testDroneCustomer.getCustomerType(), "Drone Delivery");
		assertEquals(testDroneCustomer.getLocationX(), 4);
		assertEquals(testDroneCustomer.getLocationY(), 3);
		assertEquals(testDroneCustomer.getDeliveryDistance(), 5.0, DELTA);
	}

	@Test
	public void DroneConstructor_2() throws CustomerException {
		testDroneCustomer = new DroneDeliveryCustomer("TestName123", "0123456789", -7, -3);

		assertEquals(testDroneCustomer.getName(), "TestName123");
		assertEquals(testDroneCustomer.getMobileNumber(), "0123456789");
		assertEquals(testDroneCustomer.getCustomerType(), "Drone Delivery");
		assertEquals(testDroneCustomer.getLocationX(), -7);
		assertEquals(testDroneCustomer.getLocationY(), -3);
		assertEquals(testDroneCustomer.getDeliveryDistance(), 7.62, 0.005);
	}

	@Test
	public void DroneConstructor_3() throws CustomerException {
		testDroneCustomer = new DroneDeliveryCustomer("Test O'Test", "0987654321", 10, 0);

		assertEquals(testDroneCustomer.getName(), "Test O'Test");
		assertEquals(testDroneCustomer.getMobileNumber(), "0987654321");
		assertEquals(testDroneCustomer.getCustomerType(), "Drone Delivery");
		assertEquals(testDroneCustomer.getLocationX(), 10);
		assertEquals(testDroneCustomer.getLocationY(), 0);
		assertEquals(testDroneCustomer.getDeliveryDistance(), 10, DELTA);
	}

	@Test
	public void DroneConstructor_4() throws CustomerException {
		testDroneCustomer = new DroneDeliveryCustomer("Daniel Wan", "0735681245", 0, -1);

		assertEquals(testDroneCustomer.getName(), "Daniel Wan");
		assertEquals(testDroneCustomer.getMobileNumber(), "0735681245");
		assertEquals(testDroneCustomer.getCustomerType(), "Drone Delivery");
		assertEquals(testDroneCustomer.getLocationX(), 0);
		assertEquals(testDroneCustomer.getLocationY(), -1);
		assertEquals(testDroneCustomer.getDeliveryDistance(), 1, DELTA);
	}

	@Test
	public void DroneConstructor_5() throws CustomerException {
		testDroneCustomer = new DroneDeliveryCustomer("Jason Chang", "0999999999", -10, 9);

		assertEquals(testDroneCustomer.getName(), "Jason Chang");
		assertEquals(testDroneCustomer.getMobileNumber(), "0999999999");
		assertEquals(testDroneCustomer.getCustomerType(), "Drone Delivery");
		assertEquals(testDroneCustomer.getLocationX(), -10);
		assertEquals(testDroneCustomer.getLocationY(), 9);
		assertEquals(testDroneCustomer.getDeliveryDistance(), 13.45, 0.005);
	}

	// -------- Testing Name Input --------//

	@Test(expected = CustomerException.class)
	// Minimum 1 Character
	public void EmptyName_Drone() throws CustomerException {
		testDroneCustomer = new DroneDeliveryCustomer("", "0400000000", 1, 1);
	}

	@Test
	// Minimum 1 Character
	public void Minimum_Name_Drone() throws CustomerException {
		testDroneCustomer = new DroneDeliveryCustomer("A", "0400000000", 1, 1);
	}

	@Test
	// Maximum 20 Character
	public void Maximum_Name_Drone() throws CustomerException {
		testDroneCustomer = new DroneDeliveryCustomer("AAAAAAAAAAAAAAAAAAAA", "0400000000", 1, 1);
	}

	@Test(expected = CustomerException.class)
	// Maximum 20 Character
	public void Over_Maximum_Name_Drone() throws CustomerException {
		testDroneCustomer = new DroneDeliveryCustomer("AAAAAAAAAAAAAAAAAAAAA", "0400000000", 1, 1);
	}

	@Test
	// No White spaces only for name
	public void Whitespace_and_Character_Name_Drone() throws CustomerException {
		testDroneCustomer = new DroneDeliveryCustomer("A B C D", "0400000000", 1, 1);
	}

	@Test(expected = CustomerException.class)
	// No White spaces only for name
	public void Whitespace_Name_Drone() throws CustomerException {
		testDroneCustomer = new DroneDeliveryCustomer("     ", "0400000000", 1, 1);
	}

	@Test
	// Number Name (as string) wasn't stated in task sheet digits weren't
	// allowed
	public void Number_Name_Drone() throws CustomerException {
		testDroneCustomer = new DroneDeliveryCustomer("1234", "0400000000", 1, 1);
	}

	@Test
	// Accepted Symbol (as string)
	public void Symbol_Name_Drone() throws CustomerException {
		testDroneCustomer = new DroneDeliveryCustomer("Testing O'Test", "0400000000", 1, 1);
	}

	@Test
	// Unaccepted Symbols (as string)
	public void Unaccepted_Symbol_Name_Drone() throws CustomerException {
		testDroneCustomer = new DroneDeliveryCustomer("!@#$%^&*()?><", "0400000000", 1, 1);
	}

	// -------- Testing Mobile Input --------//

	@Test
	// Valid phone number
	public void Valid_Number_Drone() throws CustomerException {
		testDroneCustomer = new DroneDeliveryCustomer("TestName", "0123456789", 1, 1);
	}

	@Test(expected = CustomerException.class)
	// mobile number must be 10 digits
	public void Under_Ten_Digit_Number_1_Drone() throws CustomerException {
		testDroneCustomer = new DroneDeliveryCustomer("TestName", "012345678", 1, 1);
	}

	@Test(expected = CustomerException.class)
	// mobile number must be 10 digits
	public void Under_Ten_Digit_Number_2_Drone() throws CustomerException {
		testDroneCustomer = new DroneDeliveryCustomer("TestName", "01234", 1, 1);
	}

	@Test(expected = CustomerException.class)
	// mobile number must be 10 digits
	public void Under_Ten_Digit_Number_3_Drone() throws CustomerException {
		testDroneCustomer = new DroneDeliveryCustomer("TestName", "0", 1, 1);
	}

	@Test(expected = CustomerException.class)
	// mobile number must be 10 digits
	public void Under_Ten_Digit_Number_4_Drone() throws CustomerException {
		testDroneCustomer = new DroneDeliveryCustomer("TestName", "", 1, 1);
	}

	@Test(expected = CustomerException.class)
	// mobile number must be 10 digits
	public void Over_Ten_Digit_Number_1_Drone() throws CustomerException {
		testDroneCustomer = new DroneDeliveryCustomer("TestName", "01234567890", 1, 1);
	}

	@Test(expected = CustomerException.class)
	// mobile number must be 10 digits
	public void Over_Ten_Digit_Number_2_Drone() throws CustomerException {
		testDroneCustomer = new DroneDeliveryCustomer("TestName", "01234567890123456789", 1, 1);
	}

	@Test(expected = CustomerException.class)
	// mobile number must start with 0
	public void Non_0_start_1_Drone() throws CustomerException {
		testDroneCustomer = new DroneDeliveryCustomer("TestName", "1234567890", 1, 1);
	}

	@Test(expected = CustomerException.class)
	// mobile number must start with 0
	public void Non_0_start_2_Drone() throws CustomerException {
		testDroneCustomer = new DroneDeliveryCustomer("TestName", "2234567890", 1, 1);
	}

	@Test(expected = CustomerException.class)
	// mobile number must start with 0
	public void Non_0_start_3_Drone() throws CustomerException {
		testDroneCustomer = new DroneDeliveryCustomer("TestName", "7234567890", 1, 1);
	}

	@Test(expected = CustomerException.class)
	// mobile number must start with 0
	public void Non_0_start_4_Drone() throws CustomerException {
		testDroneCustomer = new DroneDeliveryCustomer("TestName", "9234567890", 1, 1);
	}

	@Test(expected = CustomerException.class)
	// mobile number must all be digits
	public void All_Non_Digit_Drone() throws CustomerException {
		testDroneCustomer = new DroneDeliveryCustomer("TestName", "AAAAAAAAAA", 1, 1);
	}

	@Test(expected = CustomerException.class)
	// mobile number must all be digits
	public void Some_Non_Digit_1_Drone() throws CustomerException {
		testDroneCustomer = new DroneDeliveryCustomer("TestName", "0AAAAAAAAA", 1, 1);
	}

	@Test(expected = CustomerException.class)
	// mobile number must all be digits
	public void Some_Non_Digit_2_Drone() throws CustomerException {
		testDroneCustomer = new DroneDeliveryCustomer("TestName", "00AA0A0A00", 1, 1);
	}

	@Test(expected = CustomerException.class)
	// mobile number must all be digits
	public void Some_Non_Digit_3_Drone() throws CustomerException {
		testDroneCustomer = new DroneDeliveryCustomer("TestName", "0=A!4A5A?9", 1, 1);
	}

	// -------- Testing Location --------//

	@Test
	// Valid Location
	public void Valid_location() throws CustomerException {
		testDroneCustomer = new DroneDeliveryCustomer("TestName", "0411111111", 4, 3);
	}

	@Test(expected = CustomerException.class)
	// Will not deliver if location is 0,0
	public void Drone_invalid_location_1() throws CustomerException {
		testDroneCustomer = new DroneDeliveryCustomer("TestName", "0411111111", 0, 0);
	}

	@Test
	// Will not deliver if block distance is greater than 10
	public void Drone_max_distance_1() throws CustomerException {
		testDroneCustomer = new DroneDeliveryCustomer("TestName", "0411111111", 10, 0);
	}

	@Test
	// Will not deliver if block distance is greater than 10
	public void Drone_max_distance_2() throws CustomerException {
		testDroneCustomer = new DroneDeliveryCustomer("TestName", "0411111111", 10, 10);
	}

	@Test
	// Will not deliver if block distance is greater than 10
	public void Drone_max_distance_3() throws CustomerException {
		testDroneCustomer = new DroneDeliveryCustomer("TestName", "0411111111", -10, -10);
	}

	@Test(expected = CustomerException.class)
	// Will not deliver if block distance is greater than 10
	public void Drone_invalid_location_2() throws CustomerException {
		testDroneCustomer = new DroneDeliveryCustomer("TestName", "0411111111", -11, 0);
	}

	@Test(expected = CustomerException.class)
	// Will not deliver if block distance is greater than 10
	public void Drone_invalid_location_3() throws CustomerException {
		testDroneCustomer = new DroneDeliveryCustomer("TestName", "0411111111", 0, 11);
	}

	@Test(expected = CustomerException.class)
	// Will not deliver if block distance is greater than 10
	public void Drone_invalid_location_4() throws CustomerException {
		testDroneCustomer = new DroneDeliveryCustomer("TestName", "0411111111", 50, -50);
	}

	/********* Driver Delivery Customer *********/

	// -------- Testing Constructor --------//

	@Test
	public void DriverConstructor() throws CustomerException {
		testDriverCustomer = new DriverDeliveryCustomer("TestName", "0411111111", 4, 3);

		assertEquals(testDriverCustomer.getName(), "TestName");
		assertEquals(testDriverCustomer.getMobileNumber(), "0411111111");
		assertEquals(testDriverCustomer.getCustomerType(), "Driver Delivery");
		assertEquals(testDriverCustomer.getLocationX(), 4);
		assertEquals(testDriverCustomer.getLocationY(), 3);
		assertEquals(testDriverCustomer.getDeliveryDistance(), 7.0, DELTA);
	}

	@Test
	public void DriverConstructor2() throws CustomerException {
		testDriverCustomer = new DriverDeliveryCustomer("TestName123", "0123456789", -7, -3);

		assertEquals(testDriverCustomer.getName(), "TestName123");
		assertEquals(testDriverCustomer.getMobileNumber(), "0123456789");
		assertEquals(testDriverCustomer.getCustomerType(), "Driver Delivery");
		assertEquals(testDriverCustomer.getLocationX(), -7);
		assertEquals(testDriverCustomer.getLocationY(), -3);
		assertEquals(testDriverCustomer.getDeliveryDistance(), 10.0, DELTA);
	}

	@Test
	public void DriverConstructor3() throws CustomerException {
		testDriverCustomer = new DriverDeliveryCustomer("Test O'Test", "0987654321", 10, 0);

		assertEquals(testDriverCustomer.getName(), "Test O'Test");
		assertEquals(testDriverCustomer.getMobileNumber(), "0987654321");
		assertEquals(testDriverCustomer.getCustomerType(), "Driver Delivery");
		assertEquals(testDriverCustomer.getLocationX(), 10);
		assertEquals(testDriverCustomer.getLocationY(), 0);
		assertEquals(testDriverCustomer.getDeliveryDistance(), 10.0, DELTA);
	}

	@Test
	public void DriverConstructor4() throws CustomerException {
		testDriverCustomer = new DriverDeliveryCustomer("Daniel Wan", "0735681245", 0, -1);

		assertEquals(testDriverCustomer.getName(), "Daniel Wan");
		assertEquals(testDriverCustomer.getMobileNumber(), "0735681245");
		assertEquals(testDriverCustomer.getCustomerType(), "Driver Delivery");
		assertEquals(testDriverCustomer.getLocationX(), 0);
		assertEquals(testDriverCustomer.getLocationY(), -1);
		assertEquals(testDriverCustomer.getDeliveryDistance(), 1.0, DELTA);
	}

	@Test
	public void DriverConstructor5() throws CustomerException {
		testDriverCustomer = new DriverDeliveryCustomer("Jason Chang", "0999999999", -10, 9);

		assertEquals(testDriverCustomer.getName(), "Jason Chang");
		assertEquals(testDriverCustomer.getMobileNumber(), "0999999999");
		assertEquals(testDriverCustomer.getCustomerType(), "Driver Delivery");
		assertEquals(testDriverCustomer.getLocationX(), -10);
		assertEquals(testDriverCustomer.getLocationY(), 9);
		assertEquals(testDriverCustomer.getDeliveryDistance(), 19.0, DELTA);
	}

	// -------- Testing Name Input --------//

	@Test(expected = CustomerException.class)
	// Minimum 1 Character
	public void EmptyName_Driver() throws CustomerException {
		testDriverCustomer = new DriverDeliveryCustomer("", "0400000000", 1, 1);
	}

	@Test
	// Minimum 1 Character
	public void Minimum_Name_Driver() throws CustomerException {
		testDriverCustomer = new DriverDeliveryCustomer("A", "0400000000", 1, 1);
	}

	@Test
	// Maximum 20 Character
	public void Maximum_Name_Driver() throws CustomerException {
		testDriverCustomer = new DriverDeliveryCustomer("AAAAAAAAAAAAAAAAAAAA", "0400000000", 1, 1);
	}

	@Test(expected = CustomerException.class)
	// Maximum 20 Character
	public void Over_Maximum_Name_Driver() throws CustomerException {
		testDriverCustomer = new DriverDeliveryCustomer("AAAAAAAAAAAAAAAAAAAAA", "0400000000", 1, 1);
	}

	@Test
	// No White spaces only for name
	public void Whitespace_and_Character_Name_Driver() throws CustomerException {
		testDriverCustomer = new DriverDeliveryCustomer("A B C D", "0400000000", 1, 1);
	}

	@Test(expected = CustomerException.class)
	// No White spaces only for name
	public void Whitespace_Name_Driver() throws CustomerException {
		testDriverCustomer = new DriverDeliveryCustomer("     ", "0400000000", 1, 1);
	}

	@Test
	// Number Name (as string) wasn't stated in task sheet digits weren't
	// allowed
	public void Number_Name_Driver() throws CustomerException {
		testDriverCustomer = new DriverDeliveryCustomer("1234", "0400000000", 1, 1);
	}

	@Test
	// Accepted Symbol (as string)
	public void Symbol_Name_Driver() throws CustomerException {
		testDriverCustomer = new DriverDeliveryCustomer("Testing O'Test", "0400000000", 1, 1);
	}

	@Test
	// Unaccepted Symbols (as string)
	public void Unaccepted_Symbol_Name_Driver() throws CustomerException {
		testDriverCustomer = new DriverDeliveryCustomer("!@#$%^&*()?><", "0400000000", 1, 1);
	}

	// -------- Testing Mobile Input --------//

	@Test
	// Valid phone number
	public void Valid_Number_Driver() throws CustomerException {
		testDriverCustomer = new DriverDeliveryCustomer("TestName", "0123456789", 1, 1);
	}

	@Test(expected = CustomerException.class)
	// mobile number must be 10 digits
	public void Under_Ten_Digit_Number_1_Driver() throws CustomerException {
		testDriverCustomer = new DriverDeliveryCustomer("TestName", "012345678", 1, 1);
	}

	@Test(expected = CustomerException.class)
	// mobile number must be 10 digits
	public void Under_Ten_Digit_Number_2_Driver() throws CustomerException {
		testDriverCustomer = new DriverDeliveryCustomer("TestName", "01234", 1, 1);
	}

	@Test(expected = CustomerException.class)
	// mobile number must be 10 digits
	public void Under_Ten_Digit_Number_3_Driver() throws CustomerException {
		testDriverCustomer = new DriverDeliveryCustomer("TestName", "0", 1, 1);
	}

	@Test(expected = CustomerException.class)
	// mobile number must be 10 digits
	public void Under_Ten_Digit_Number_4_Driver() throws CustomerException {
		testDriverCustomer = new DriverDeliveryCustomer("TestName", "", 1, 1);
	}

	@Test(expected = CustomerException.class)
	// mobile number must be 10 digits
	public void Over_Ten_Digit_Number_1_Driver() throws CustomerException {
		testDriverCustomer = new DriverDeliveryCustomer("TestName", "01234567890", 1, 1);
	}

	@Test(expected = CustomerException.class)
	// mobile number must be 10 digits
	public void Over_Ten_Digit_Number_2_Driver() throws CustomerException {
		testDriverCustomer = new DriverDeliveryCustomer("TestName", "01234567890123456789", 1, 1);
	}

	@Test(expected = CustomerException.class)
	// mobile number must start with 0
	public void Non_0_start_1_Driver() throws CustomerException {
		testDriverCustomer = new DriverDeliveryCustomer("TestName", "1234567890", 1, 1);
	}

	@Test(expected = CustomerException.class)
	// mobile number must start with 0
	public void Non_0_start_2_Driver() throws CustomerException {
		testDriverCustomer = new DriverDeliveryCustomer("TestName", "2234567890", 1, 1);
	}

	@Test(expected = CustomerException.class)
	// mobile number must start with 0
	public void Non_0_start_3_Driver() throws CustomerException {
		testDriverCustomer = new DriverDeliveryCustomer("TestName", "7234567890", 1, 1);
	}

	@Test(expected = CustomerException.class)
	// mobile number must start with 0
	public void Non_0_start_4_Driver() throws CustomerException {
		testDriverCustomer = new DriverDeliveryCustomer("TestName", "9234567890", 1, 1);
	}

	@Test(expected = CustomerException.class)
	// mobile number must all be digits
	public void All_Non_Digit_Driver() throws CustomerException {
		testDriverCustomer = new DriverDeliveryCustomer("TestName", "AAAAAAAAAA", 1, 1);
	}

	@Test(expected = CustomerException.class)
	// mobile number must all be digits
	public void Some_Non_Digit_1_Driver() throws CustomerException {
		testDriverCustomer = new DriverDeliveryCustomer("TestName", "0AAAAAAAAA", 1, 1);
	}

	@Test(expected = CustomerException.class)
	// mobile number must all be digits
	public void Some_Non_Digit_2_Driver() throws CustomerException {
		testDriverCustomer = new DriverDeliveryCustomer("TestName", "00AA0A0A00", 1, 1);
	}

	@Test(expected = CustomerException.class)
	// mobile number must all be digits
	public void Some_Non_Digit_3_Driver() throws CustomerException {
		testDriverCustomer = new DriverDeliveryCustomer("TestName", "0=A!4A5A?9", 1, 1);
	}

	// -------- Testing Location --------//

	@Test
	// Valid Location
	public void Valid_location_Driver() throws CustomerException {
		testDriverCustomer = new DriverDeliveryCustomer("TestName", "0411111111", 4, 3);
	}

	@Test(expected = CustomerException.class)
	// Will not deliver if location is 0,0
	public void Driver_invalid_location_1() throws CustomerException {
		testDriverCustomer = new DriverDeliveryCustomer("TestName", "0411111111", 0, 0);
	}

	@Test
	// Will not deliver if block distance is greater than 10
	public void Driver_max_distance_1() throws CustomerException {
		testDriverCustomer = new DriverDeliveryCustomer("TestName", "0411111111", 10, 0);
	}

	@Test
	// Will not deliver if block distance is greater than 10
	public void Driver_max_distance_2() throws CustomerException {
		testDriverCustomer = new DriverDeliveryCustomer("TestName", "0411111111", 10, 10);
	}

	@Test
	// Will not deliver if block distance is greater than 10
	public void Driver_max_distance_3() throws CustomerException {
		testDriverCustomer = new DriverDeliveryCustomer("TestName", "0411111111", -10, -10);
	}

	@Test(expected = CustomerException.class)
	// Will not deliver if block distance is greater than 10
	public void Driver_invalid_location_2() throws CustomerException {
		testDriverCustomer = new DriverDeliveryCustomer("TestName", "0411111111", -11, 0);
	}

	@Test(expected = CustomerException.class)
	// Will not deliver if block distance is greater than 10
	public void Driver_invalid_location_3() throws CustomerException {
		testDriverCustomer = new DriverDeliveryCustomer("TestName", "0411111111", 0, 11);
	}

	@Test(expected = CustomerException.class)
	// Will not deliver if block distance is greater than 10
	public void Driver_invalid_location_4() throws CustomerException {
		testDriverCustomer = new DriverDeliveryCustomer("TestName", "0411111111", 50, -50);
	}
}
