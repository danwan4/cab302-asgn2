package asgn2Tests;

import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.Test;

import asgn2Exceptions.*;
import asgn2Customers.*;
import asgn2Restaurant.LogHandler;

/**
 * A class that tests the methods relating to the creation of Customer objects
 * in the asgn2Restaurant.LogHander class.
 *
 * @author Jason Chang
 */
public class LogHandlerCustomerTests {

	// -------- Testing Obtaining Data Set --------//

	@Test
	public void PopulateCustomerDataset1() throws CustomerException, LogHandlerException {
		ArrayList<Customer> customer = LogHandler.populateCustomerDataset(".//logs/20170101.txt");
		assertEquals(customer.size(), 3);
	}

	@Test
	public void PopulateCustomerDataset2() throws CustomerException, LogHandlerException {
		ArrayList<Customer> customer = LogHandler.populateCustomerDataset(".//logs/20170102.txt");
		assertEquals(customer.size(), 10);
	}

	@Test
	public void PopulateCustomerDataset3() throws CustomerException, LogHandlerException {
		ArrayList<Customer> customer = LogHandler.populateCustomerDataset(".//logs/20170103.txt");
		assertEquals(customer.size(), 100);
	}

	// -------- Testing Creating Customer Log --------//

	@Test
	public void CreateCustomer1() throws CustomerException, LogHandlerException {
		PickUpCustomer expectedCustomer = new PickUpCustomer("Oroku Saki", "0111222333", 0, 0);
		Customer outputCustomer = LogHandler.createCustomer("21:00:00,21:35:00,Oroku Saki,0111222333,PUC,0,0,PZL,3");
		assertEquals(expectedCustomer, outputCustomer);
	}

	@Test
	public void CreateCustomer2() throws CustomerException, LogHandlerException {
		DroneDeliveryCustomer expectedCustomer = new DroneDeliveryCustomer("April O'Neal", "0987654321", 3, 4);
		Customer outputCustomer = LogHandler.createCustomer("20:00:00,20:25:00,April O'Neal,0987654321,DNC,3,4,PZM,1");
		assertEquals(expectedCustomer, outputCustomer);
	}

	@Test
	public void CreateCustomer3() throws CustomerException, LogHandlerException {
		DriverDeliveryCustomer expectedCustomer = new DriverDeliveryCustomer("Casey Jones", "0123456789", 5, 5);
		Customer outputCustomer = LogHandler.createCustomer("19:00:00,19:20:00,Casey Jones,0123456789,DVC,5,5,PZV,2");
		assertEquals(expectedCustomer, outputCustomer);
	}

	@Test(expected = LogHandlerException.class)
	public void emptyString() throws CustomerException, LogHandlerException {
		LogHandler.createCustomer("");
	}

	@Test(expected = LogHandlerException.class)
	public void not_Enough_Values() throws CustomerException, LogHandlerException {
		LogHandler.createCustomer("19:00:00,19:20:00,0123456789,DVC,5,2");
	}

	@Test(expected = LogHandlerException.class)
	public void Too_Many_Values() throws CustomerException, LogHandlerException {
		LogHandler.createCustomer("19:00:00,19:20:00,Casey Jones,0123456789,DVC,5,5,PZV,2,hello,1,2");
	}

	@Test(expected = LogHandlerException.class)
	public void No_Seperation() throws CustomerException, LogHandlerException {
		LogHandler.createCustomer("19:00:0019:20:00Casey Jones0123456789DVC5PZV2");
	}

	@Test(expected = LogHandlerException.class)
	public void Invalid_X_Location() throws CustomerException, LogHandlerException {
		LogHandler.createCustomer("19:00:00,19:20:00,Casey Jones,0123456789,DVC,Casey Jones,5,PZV,2");
	}

	@Test(expected = LogHandlerException.class)
	public void Invalid_Y_Location() throws CustomerException, LogHandlerException {
		LogHandler.createCustomer("19:00:00,19:20:00,Casey Jones,0123456789,DVC,5,Casey Jones,PZV,2");
	}

	@Test(expected = LogHandlerException.class)
	public void Invalid_Values_Combination() throws CustomerException, LogHandlerException {
		LogHandler.createCustomer("19:00:00,19:20:00,Casey Jones,0123456789,DVC,Casey Jones,Casey Jones,PZV,2");
	}

	@Test(expected = CustomerException.class)
	// Too many Characters in Name
	public void Invalid_Name_1() throws CustomerException, LogHandlerException {
		LogHandler.createCustomer("19:00:00,19:20:00,AAAAAAAAAAAAAAAAAAAAAA,0123456789,DVC,5,5,PZV,2");
	}

	@Test(expected = CustomerException.class)
	// Not enough Characters in Name
	public void Invalid_Name_2() throws CustomerException, LogHandlerException {
		LogHandler.createCustomer("19:00:00,19:20:00, ,0123456789,DVC,5,5,PZV,2");
	}

	@Test(expected = CustomerException.class)
	// Invalid Characters in Name
	public void Invalid_Name_3() throws CustomerException, LogHandlerException {
		LogHandler.createCustomer("19:00:00,19:20:00,,0123456789,DVC,5,5,PZV,2");
	}

	@Test(expected = CustomerException.class)
	// Too many digits in mobile number
	public void Invalid_Mobile_1() throws CustomerException, LogHandlerException {
		LogHandler.createCustomer("19:00:00,19:20:00,Casey Jones,01234567891,DVC,5,5,PZV,2");
	}

	@Test(expected = CustomerException.class)
	// Not enough digits in mobile number
	public void Invalid_Mobile_2() throws CustomerException, LogHandlerException {
		LogHandler.createCustomer("19:00:00,19:20:00,Casey Jones,012345678,DVC,5,5,PZV,2");
	}

	@Test(expected = CustomerException.class)
	// Mobile doesnt start with 0
	public void Invalid_Mobile_3() throws CustomerException, LogHandlerException {
		LogHandler.createCustomer("19:00:00,19:20:00,Casey Jones,11234567891,DVC,5,5,PZV,2");
	}

	@Test(expected = CustomerException.class)
	// Mobile must be all digits
	public void Invalid_Mobile_4() throws CustomerException, LogHandlerException {
		LogHandler.createCustomer("19:00:00,19:20:00,Casey Jones,000000AAAA,DVC,5,5,PZV,2");
	}

	@Test(expected = CustomerException.class)
	// Customer Type must be DVC,DNC or PUC
	public void Invalid_Customer_Type_1() throws CustomerException, LogHandlerException {
		LogHandler.createCustomer("19:00:00,19:20:00,Casey Jones,0123456789,ABC,5,5,PZV,2");
	}

	@Test(expected = CustomerException.class)
	// Customer Type must be DVC,DNC or PUC
	public void Invalid_Customer_Type_2() throws CustomerException, LogHandlerException {
		LogHandler.createCustomer("19:00:00,19:20:00,Casey Jones,0123456789,123,5,5,PZV,2");
	}

	@Test(expected = CustomerException.class)
	// Customer Type must be DVC,DNC or PUC
	public void Invalid_Customer_Type_3() throws CustomerException, LogHandlerException {
		LogHandler.createCustomer("19:00:00,19:20:00,Casey Jones,0123456789,!@#,5,5,PZV,2");
	}

	@Test(expected = CustomerException.class)
	// Customer Type must be DVC,DNC or PUC
	public void Invalid_Customer_Type_4() throws CustomerException, LogHandlerException {
		LogHandler.createCustomer("19:00:00,19:20:00,Casey Jones,0123456789,,5,5,PZV,2");
	}

	@Test(expected = CustomerException.class)
	// Location must be within 10 blocks to deliver
	public void Invalid_Location_1() throws CustomerException, LogHandlerException {
		LogHandler.createCustomer("19:00:00,19:20:00,Casey Jones,0123456789,DVC,-11,11,PZV,2");
	}

	@Test(expected = CustomerException.class)
	// Location cannot be 0,0 to deliver
	public void Invalid_Location_2() throws CustomerException, LogHandlerException {
		LogHandler.createCustomer("19:00:00,19:20:00,Casey Jones,0123456789,DVC,0,0,PZV,2");
	}

	@Test(expected = CustomerException.class)
	// Location must be 0,0 to pick up
	public void Invalid_Location_3() throws CustomerException, LogHandlerException {
		LogHandler.createCustomer("19:00:00,19:20:00,Casey Jones,0123456789,PUC,1,1,PZV,2");
	}
}
