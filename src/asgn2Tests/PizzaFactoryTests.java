package asgn2Tests;

import static org.junit.Assert.*;

import org.junit.Test;

import java.time.LocalTime;

import asgn2Exceptions.PizzaException;
import asgn2Pizzas.*;

/**
 * A class that tests the asgn2Pizzas.PizzaFactory class.
 * 
 * @author Daniel Wan
 * 
 */

public class PizzaFactoryTests {

	@Test
	public void meatlovers() throws PizzaException {
		MeatLoversPizza expectedPizza = new MeatLoversPizza(5, LocalTime.of(19, 0), LocalTime.of(19, 30));
		Pizza outputPizza = PizzaFactory.getPizza("PZL", 5, LocalTime.of(19, 0), LocalTime.of(19, 30));
		assertEquals(expectedPizza, outputPizza);
	}

	@Test
	public void margherita() throws PizzaException {
		MargheritaPizza expectedPizza = new MargheritaPizza(5, LocalTime.of(19, 0), LocalTime.of(19, 30));
		Pizza outputPizza = PizzaFactory.getPizza("PZM", 5, LocalTime.of(19, 0), LocalTime.of(19, 30));
		assertEquals(expectedPizza, outputPizza);
	}

	@Test
	public void vegetarian() throws PizzaException {
		VegetarianPizza expectedPizza = new VegetarianPizza(5, LocalTime.of(19, 0), LocalTime.of(19, 30));
		Pizza outputPizza = PizzaFactory.getPizza("PZV", 5, LocalTime.of(19, 0), LocalTime.of(19, 30));
		assertEquals(expectedPizza, outputPizza);
	}

	@Test
	public void differentToppings() throws PizzaException {
		MeatLoversPizza expectedPizza = new MeatLoversPizza(5, LocalTime.of(19, 0), LocalTime.of(19, 30));
		Pizza outputPizza = PizzaFactory.getPizza("PZV", 5, LocalTime.of(19, 0), LocalTime.of(19, 30));
		assertNotEquals(expectedPizza, outputPizza);
	}

	@Test
	public void differentQuantities() throws PizzaException {
		MeatLoversPizza expectedPizza = new MeatLoversPizza(5, LocalTime.of(19, 0), LocalTime.of(19, 30));
		Pizza outputPizza = PizzaFactory.getPizza("PZM", 3, LocalTime.of(19, 0), LocalTime.of(19, 30));
		assertNotEquals(expectedPizza, outputPizza);
	}

	@Test(expected = PizzaException.class)
	public void invalidPizzaCode1() throws PizzaException {
		PizzaFactory.getPizza("LUL", 5, LocalTime.of(19, 0), LocalTime.of(19, 30));
	}

	@Test(expected = PizzaException.class)
	public void invalidPizzaCode2() throws PizzaException {
		PizzaFactory.getPizza("123", 5, LocalTime.of(19, 0), LocalTime.of(19, 30));
	}

	@Test(expected = PizzaException.class)
	public void invalidPizzaCode3() throws PizzaException {
		PizzaFactory.getPizza("", 5, LocalTime.of(19, 0), LocalTime.of(19, 30));
	}

	@Test(expected = PizzaException.class)
	public void invalidQuantity() throws PizzaException {
		PizzaFactory.getPizza("PZL", 11, LocalTime.of(19, 00), LocalTime.of(19, 15));
	}

	@Test(expected = PizzaException.class)
	public void invalidOrderTime() throws PizzaException {
		PizzaFactory.getPizza("PZL", 5, LocalTime.of(18, 30), LocalTime.of(19, 15));
	}

	@Test(expected = PizzaException.class)
	public void invalidDeliveryTime() throws PizzaException {
		PizzaFactory.getPizza("PZL", 5, LocalTime.of(19, 30), LocalTime.of(19, 15));
	}
}
