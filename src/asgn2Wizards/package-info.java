/**
 * This package contains the entry point to the rest of the system
 *
 * @author Jason Chang and Daniel Wan
 *
 */
package asgn2Wizards;