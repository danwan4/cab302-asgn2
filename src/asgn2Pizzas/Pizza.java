package asgn2Pizzas;

import asgn2Exceptions.PizzaException;
import java.time.LocalTime;

/**
 * An abstract class that represents pizzas sold at the Pizza Palace restaurant.
 * The Pizza class is used as a base class of VegetarianPizza, MargheritaPizza
 * and MeatLoversPizza. Each of these subclasses have a different set of
 * toppings. A description of the class's fields and their constraints is
 * provided in Section 5.1 of the Assignment Specification.
 * 
 * @author Jason Chang
 *
 */
public abstract class Pizza {

	private int quantity;
	private LocalTime orderTime;
	private LocalTime deliveryTime;
	private String type;
	private double price;
	private double cost;

	/**
	 * This class represents a pizza produced at the Pizza Palace restaurant. A
	 * detailed description of the class's fields and parameters is provided in
	 * the Assignment Specification, in particular in Section 5.1. A
	 * PizzaException is thrown if the any of the constraints listed in Section
	 * 5.1 of the Assignment Specification are violated.
	 *
	 * PRE: TRUE POST: All field values except cost per pizza are set
	 * 
	 * @param quantity
	 *            - The number of pizzas ordered
	 * @param orderTime
	 *            - The time that the pizza order was made and sent to the
	 *            kitchen
	 * @param deliveryTime
	 *            - The time that the pizza was delivered to the customer
	 * @param type
	 *            - A human understandable description of this Pizza type
	 * @param price
	 *            - The price that the pizza is sold to the customer
	 * @throws PizzaException
	 *             - If the quantity is not between 1 and 10
	 *             - If the order time is not between 7pm and 11pm
	 *             - If the delivery time is not at between 10 to 60 minutes after order time
	 * 
	 */

	public Pizza(int quantity, LocalTime orderTime, LocalTime deliveryTime, String type, double price)
			throws PizzaException {
		// quantity
		if (quantity > 10) {
			throw new PizzaException("Cannot place more than 10 pizzas in one order");
		}
		if (quantity < 1) {
			throw new PizzaException("Must place at least one pizza in the order");
		}

		// OrderTime
		if (orderTime.isBefore(LocalTime.of(19, 0, 0, 0))) {
			throw new PizzaException("Cannot place an order before 7pm");
		}
		if (orderTime.isAfter(LocalTime.of(23, 0, 0, 0))) {
			throw new PizzaException("Cannot place an order after 11pm");
		}

		// deliveryTime
		if (deliveryTime.isBefore(orderTime)) {
			throw new PizzaException("Cannot deliver before order time");
		}
		if (deliveryTime.minusMinutes(10).isBefore(orderTime)) {
			throw new PizzaException("Cannot deliver while pizza is still cooking");
		}
		if ((deliveryTime.minusHours(1)).isAfter(orderTime)) {
			throw new PizzaException("Pizza has been thrown out");
		}

		this.quantity = quantity;
		this.orderTime = orderTime;
		this.deliveryTime = deliveryTime;
		this.type = type;
		this.price = price;
	}

	/**
	 * Calculates how much a pizza would cost to make calculated from its
	 * toppings.
	 * 
	 * <P>
	 * PRE: TRUE
	 * <P>
	 * POST: The cost field is set to sum of the Pizzas's toppings
	 */
	public final void calculateCostPerPizza() {
		final double Cheese = PizzaTopping.CHEESE.getCost();
		final double Tomato = PizzaTopping.TOMATO.getCost();
		final double Bacon = PizzaTopping.BACON.getCost();
		final double Salami = PizzaTopping.SALAMI.getCost();
		final double Pepperoni = PizzaTopping.PEPPERONI.getCost();
		final double Capsicum = PizzaTopping.CAPSICUM.getCost();
		final double Mushroom = PizzaTopping.MUSHROOM.getCost();
		final double Eggplant = PizzaTopping.EGGPLANT.getCost();

		if (this.type.equals("Margherita")) {
			this.cost = Tomato + Cheese;
		} else if (this.type.equals("Vegetarian")) {
			this.cost = Tomato + Cheese + Eggplant + Mushroom + Capsicum;
		} else if (this.type.equals("Meat Lovers")) {
			this.cost = Tomato + Cheese + Bacon + Pepperoni + Salami;
		}
	}

	/**
	 * Returns the amount that an individual pizza costs to make.
	 * 
	 * @return The amount that an individual pizza costs to make.
	 */
	public final double getCostPerPizza() {
		return cost;

	}

	/**
	 * Returns the amount that an individual pizza is sold to the customer.
	 * 
	 * @return The amount that an individual pizza is sold to the customer.
	 */
	public final double getPricePerPizza() {
		return price;
	}

	/**
	 * Returns the amount that the entire order costs to make, taking into
	 * account the type and quantity of pizzas.
	 * 
	 * @return The amount that the entire order costs to make, taking into
	 *         account the type and quantity of pizzas.
	 */
	public final double getOrderCost() {
		return cost * quantity;
	}

	/**
	 * Returns the amount that the entire order is sold to the customer, taking
	 * into account the type and quantity of pizzas.
	 * 
	 * @return The amount that the entire order is sold to the customer, taking
	 *         into account the type and quantity of pizzas.
	 */
	public final double getOrderPrice() {
		return getPricePerPizza() * quantity;
	}

	/**
	 * Returns the profit made by the restaurant on the order which is the order
	 * price minus the order cost.
	 * 
	 * @return Returns the profit made by the restaurant on the order which is
	 *         the order price minus the order cost.
	 */
	public final double getOrderProfit() {
		return getOrderPrice() - getOrderCost();
	}

	/**
	 * Indicates if the pizza contains the specified pizza topping or not.
	 * 
	 * @param topping
	 *            - A topping as specified in the enumeration PizzaTopping
	 * @return Returns true if the instance of Pizza contains the specified
	 *         topping and false otherwise.
	 */
	public final boolean containsTopping(PizzaTopping topping) {
		if (type.equals("Margherita")) {
			if (topping.equals(PizzaTopping.CHEESE) || topping.equals(PizzaTopping.TOMATO)) {
				return true;
			}
			return false;
		} else if (type.equals("Vegetarian")) {
			if (topping.equals(PizzaTopping.CHEESE) || topping.equals(PizzaTopping.TOMATO)
					|| topping.equals(PizzaTopping.EGGPLANT) || topping.equals(PizzaTopping.MUSHROOM)
					|| topping.equals(PizzaTopping.CAPSICUM)) {
				return true;
			}
			return false;
		} else if (type.equals("Meat Lovers")) {
			if (topping.equals(PizzaTopping.CHEESE) || topping.equals(PizzaTopping.TOMATO)
					|| topping.equals(PizzaTopping.BACON) || topping.equals(PizzaTopping.PEPPERONI)
					|| topping.equals(PizzaTopping.SALAMI)) {
				return true;
			}
			return false;
		}
		return false;
	}

	/**
	 * Returns the quantity of pizzas ordered.
	 * 
	 * @return the quantity of pizzas ordered.
	 */
	public final int getQuantity() {
		return quantity;
	}

	/**
	 * Returns a human understandable description of the Pizza's type. The valid
	 * alternatives are listed in Section 5.1 of the Assignment Specification.
	 * 
	 * @return A human understandable description of the Pizza's type.
	 */
	public final String getPizzaType() {
		return type;
	}

	/**
	 * Compares *this* Pizza object with an instance of an *other* Pizza object
	 * and returns true if if the two objects are equivalent, that is, if the
	 * values exposed by public methods are equal. You do not need to test this
	 * method.
	 * 
	 * @return true if *this* Pizza object and the *other* Pizza object have the
	 *         same values returned for getCostPerPizza(), getOrderCost(),
	 *         getOrderPrice(), getOrderProfit(), getPizzaType(),
	 *         getPricePerPizza() and getQuantity().
	 * 
	 */
	@Override
	public boolean equals(Object other) {
		Pizza otherPizza = (Pizza) other;
		return ((this.getCostPerPizza()) == (otherPizza.getCostPerPizza())
				&& (this.getOrderCost()) == (otherPizza.getOrderCost()))
				&& (this.getOrderPrice()) == (otherPizza.getOrderPrice())
				&& (this.getOrderProfit()) == (otherPizza.getOrderProfit())
				&& (this.getPizzaType() == (otherPizza.getPizzaType())
						&& (this.getPricePerPizza()) == (otherPizza.getPricePerPizza())
						&& (this.getQuantity()) == (otherPizza.getQuantity()));
	}

}
